"use strict";
let express = require('express');
let router = express.Router();

let user = {
    name: "Danil",
    secondName: "Kazimirov",
    almamater: "KPI",
};
/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('about', user);
});

module.exports = router;
