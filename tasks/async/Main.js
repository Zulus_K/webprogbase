"use strict";
const fs = require('fs-promise');
const XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

function askQuestion() {
    process.stdout.write('Input city name: ');
}

function request(method, url) {
    return new Promise(function (resolve, reject) {
        let xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        xhr.send();
        xhr.onload = () => {
            if (xhr.status >= 200 && xhr.status <= 300) {
                resolve(xhr.responseText);
            } else {
                //create error
                reject(
                    {
                        status: xhr.status,
                        message: xhr.statusText
                    }
                );
            }
        };
        xhr.onerror = () => reject(
            {
                status: xhr.status,
                message: xhr.statusText
            }
        );
    });
}

function parseResponseApi(text) {
    try {
        return JSON.parse(text)["_embedded"]["city:search-results"][0]["_links"]["city:item"]["href"];
    } catch (e) {
        return {status: -1, message: "error at parse"};
    }
}

function save(name, text) {
    //run async saving
    fs.writeFile(name + ".txt", text.trim())
        .then(() => {
            console.log("success saved to '" + name + ".txt'");
            console.log(`Text: '${text.trim()}'`);
        })
        .catch("cannot save to '" + name + ".txt'");
}

function printError(e) {
    console.log("error", "\nstatus: " + e.status, "\nmessage: " + e.message.toString());
}

function apiRequestTeleport(cityName) {
    console.log("Send request to 'teleport.org'");
    request('GET', 'https://api.teleport.org/api/cities/?search=' + cityName)
        .then((text) => {
            console.log("success connection to 'teleport.org'");
            //parse json answer
            let url = parseResponseApi(text);
            //if success, send new request
            if (url) {
                console.log(`Send request to '${url}'`);
                request('GET', url)
                    .then((text) => save(cityName, text))
                    .catch(printError);
            }
        }).catch(printError);

}

function processInput(buffer) {
    let inputString = buffer.toString().trim();
    console.log(`Entered city: '${inputString}'`);
    //if string is present
    if (inputString) {
        fs.readFile(inputString + ".txt")
            .then(fileText => {
                console.log(`Find file: '${inputString + ".txt"}'`);
                console.log(`Text: '${fileText.toString().trim()}'`);
            })
            .catch(() => {
                console.log(`Cannot find file: '${inputString + ".txt"}'`);
                apiRequestTeleport(inputString);
            }).then(() => askQuestion());
    } else {
        console.log(`Exit.`);
        process.stdin.end();  // stop listening input
    }
}

process.stdin.addListener('data', processInput);

//ask user input
askQuestion();
