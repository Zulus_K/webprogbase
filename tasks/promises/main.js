'use strict';
let fetch = require('node-fetch');
let fs = require('fs');

/**
 * parse string and return city object
 * @param jobj -response
 * @returns {city}
 */
function parseCityInfo(jobj) {
    try {
        let city = {};
        city.id = Number(jobj['geoname_id']);
        city.name = jobj['name'];
        city.full_name = jobj['full_name'];
        city.country = jobj['_links']['city:country']['name'];
        city.population = Number(jobj['population']);
        city.location = {
            latitude: Number(jobj['location']['latlon']['latitude']),
            longitude: Number(jobj['location']['latlon']['longitude'])
        };
        city.toString = function () {
            return this.name
        };
        return city;
    }
    catch
        (e) {
        console.log('Error at parse:\n' + e);
        return {};
    }
}

/**
 * sends req to site and return promise of connection
 * @param cityName - name of city
 * @returns {Promise} promise of connection
 */
function getCityInfo(cityName) {
    if (!cityName) return;
    return new Promise((resolve, reject) => {
        //send req to teleport.org
        //if success, send req to site with this city
        try {
            fetch('https://api.teleport.org/api/cities/?search=' + cityName)
                .then((res) => {
                    return res.json();
                }).then((text) => {
                //get city id-ref
                let cityHref = text['_embedded']['city:search-results'][0]['_links']['city:item']['href'];
                //send req with city id and return promise
                return fetch(cityHref);
            })
                .then((res) => {
                    return res.json()
                })
                .then((text) => {
                    resolve(parseCityInfo(text));
                })
                .catch((err) => {
                    reject(err)
                });
        } catch (err) {
            reject(err);
        }
    });
}

/**
 * read file and return promise with data
 * @param fileName name of file to read
 * @returns {Promise} promise with read file
 */
function readFile(fileName) {
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data.toString());
            }
        });
    });
}

//
// getCityInfo('Kiev').then((city) => {
//     console.log(JSON.stringify(city));
// }).catch((err) => {
//     console.log(err);
// });
readFile('data.txt').then((data) => {
    let allCity = [];
    for (let city of data.split(/,\s?/)) {
        allCity.push(getCityInfo(city));
    }
    return allCity;
}).then((allCity) => {
    //load all city async
    Promise.all(allCity).then((arr) => {
        //sort by population
        arr.sort((a, b) => {
                return b.population - a.population;
            }
        );
        return arr;
    }).then((allCities) => {
        //create dictionaries of arrays with cities
        let dictionary = {};
        for (let city of allCities) {
            if (dictionary[city.country] !== undefined) {
                dictionary[city.country].push(city);
            } else {
                dictionary[city.country] = [city];
            }
        }
        return dictionary;
    }).then((dic) => {
        //write all cities
        for (let country in dic) {
            console.log(country + ":" + dic[country].join(", "));
        }
    }).catch((err) => console.log(err));
});