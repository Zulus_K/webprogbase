const DBuser = require("@DB/controller.users");
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const session = require('express-session');
const Utils = require('@utils')
const JWT = require('jsonwebtoken');
const config = require('@config')
const local = {
    validate: async function (email, password, done) {
        try {
            const user = await DBuser.findByEmail(email);
            if (user && user.comparePassword(password)) {
                done(null, user);
            } else {
                console.log(`-: local.auth, no such user:  ${email}: ${password}`);
                done('error, not registred yet');
            }
        } catch (e) {
            console.log(`-error: ${e}`);
            done(e);
        }
    },
    configure: function (app) {
        app.use(session({
            secret: config.SECRET,
            resave: false,
            saveUninitialized: true
        }));
        app.use(passport.initialize());
        app.use(passport.session());
        passport.use(new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            session: true
        }, local.validate));
        passport.serializeUser(function (user, done) {
            done(null, user._id);
        });
        passport.deserializeUser(async function (id, done) {
            try {
                let user = await DBuser.getById(id);
                if (!user) {
                    done(`no such user ${id}`);
                } else {
                    done(null, user);
                }
            } catch (e) {
                console.log(`-passport error at deserialize ${e}`)
                done(e, null);
            }
        });
    }
}

const jwt = {
    checkHeaders: function (req) {
        return req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer' && req.headers.authorization.split(' ')[1]
    },
    loadUser: async function (req, res, next, payload, name, token) {
        try {
            // get user from db
            const user = await DBuser.getById(payload.id);
            // check tokens
            if (user && user.checkToken(name, token)) {
                req.user = user;
            } else {
                res.user = undefined;
            }
        } catch (e) {
            req.user = undefined;
        }
    },
    processError: async function (req, res, next, err) {
        if (err.name === 'TokenExpiredError') {
            // old token
            req.user = undefined;
            return Utils.errorAPI(res, 401, "TokenExpiredError");
        } else {
            // some else error
            req.user = undefined;
            next();
        }
    },
    configure: function (app) {
        app.use(function (req, res, next) {
            if (!req.user && jwt.checkHeaders(req)) {
                const token = req.headers.authorization.split(' ')[1];
                JWT.verify(token, process.env.SECRET, async function (err, decode) {
                    if (!err) {
                        await jwt.loadUser(req, res, next, decode, 'access', token)
                        return next();
                    } else {
                        return await jwt.processError(req, res, next, err)
                    }
                })
            } else {
                return next();
            }
        });
    }
}

exports.local = {
    checkAuth: function checkAuth (req, res, next) {
        if (!req.user) {
            console.log("user does't authorized");
            let err = new Error('Authorization required');
            err.status = 401;
            next(err);
        }
        next();
    },
    authenticate: function (success, fail) {
        return passport.authenticate('local', {
            successRedirect: success,
            failureRedirect: fail
        });
    },
    authorize: passport.authenticate('local'),
    isAuthorized: function (req) {
        if (!req.user) {
            console.log("user does't authorized");
            return false;
        }
        return true;
    }
}

exports.jwt = {
    // check token
    // check refresh
    // init
    //add jwt to app
    token: function (type, user) {
        switch (type) {
            case 'jwt':
                return JWT.sign({
                    email: user.email,
                    id: user._id
                }, config.SECRET, {expiresIn: config.TOKEN_EXP_IN_ACCESS})
                break;
            case 'refresh':
                return JWT.sign({
                    id: user.id,
                }, config.SECRET_REFRESH, {expiresIn: config.TOKEN_EXP_IN_REFRESH})
                break;
        }
    },
    check: {
        login: function (req, res, next) {
            if (req.user) {
                next();
            } else {
                return res.status(401).json({success: false, message: "Unauthorized"});
            }
        },
        admin: function (req, res, next) {
            if (req.user && req.user.isAdmin) {
                next();
            } else {
                return res.status(403).json({success: false, message: "Access denied"});
            }
        },
        refresh: async function (req, res, next) {
            if (jwt.checkHeaders(req)) {
                const token = req.headers.authorization.split(' ')[1];
                //load user
                JWT.verify(token, config.SECRET_REFRESH, async function (err, payload) {
                    if (err) {
                        return Utils.errorAPI(res, 400, "Invalid token");
                    } else {
                        await jwt.loadUser(req, res, next, payload, 'refresh', token);
                        if (req.user) {
                            return next();
                        } else {
                            return Utils.errorAPI(res, 401, "TokenExpiredError");
                        }
                    }
                })
            } else {
                return Utils.errorAPI(res, 400, "No tokens found in req body");
            }
        }
    },
}
exports.init = function (app) {
    local.configure(app);
    jwt.configure(app);
}
exports.checkAdmin = function (req, res, next) {
    if (!req.user) {
        console.log("user does't authorized");
        let err = new Error('Authorization required');
        err.status = 401;
        next(err);
    }
    if (!req.user.isAdmin) {
        let err = new Error('Admin access required');
        err.status = 401;
        next(err);
    } else {
        next();
    }
}