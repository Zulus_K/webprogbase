const path=require('path');

module.exports = {
    db: `mongodb://localhost:27017/webprogbase`,
    LIMIT: Number(process.env.LIMIT) || 7,
    RANGE: Number(process.env.RANGE) || 3,
    SALT: process.env.SALT || "RAISAWESOME",
    SECRET: process.env.SECRET || 'RazDwaTriAshkuPoluchi',
    SECRET_REFRESH: process.env.SECRET_REFRESH || 'AshkuMne',
    PORT: Number(process.env.PORT) || 2000,
    TOKEN_EXP_IN_ACCESS: process.env.TOKEN_EXP_IN_ACCESS || '1h',
    TOKEN_EXP_IN_REFRESH: process.env.TOKEN_EXP_IN_REFRESH || '1d',
    validate:require('@validator'),
    errors:{
        INVALID_VALUE:1
    },
    APIroad:path.join(__dirname,'..','endpoints.md')
}