require('dotenv').config();
require('module-alias/register');
// drivers of DB
const DBimage = require('./storage/controller.image');
const DBdriver = require('./storage/controller');
const DBuser = require('./storage/controller.users');
const DBpublications = require('./storage/controller.users');
// tools
const express = require('express');
const path = require('path');
const logger = require('morgan');
const favicon = require('serve-favicon');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
// config
const config = require('./utils/config')

// app
app = express();
// routes
const index = require('@routes/index');
const auth = require('@routes/auth');
const users = require('@routes/users');
const publications = require('@routes/publications');
const resource = require('@routes/resource');
const api = require('@routes/api');

console.log('+:Routes loaded')

async function createConnections () {
    try {
        let result = await DBdriver.connect(config.db);
        console.log('+:Connected to DB');
        await  DBimage.connect();
        console.log('+:Connected to image storage ');
    } catch (e) {
        console.log('-:Cannot connected to mongodb ' + (e ? e : ""));
        process.exit(0);
    }
}

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(busboyBodyParser());
app.use(cookieParser());

// init passport
require('./utils/auth').init(app);
console.log('+:Passport configured');

app.use('/', index);
app.use('/publications', publications);
app.use('/res', resource);
app.use('/auth', auth);
app.use('/users', users);
app.use('/api', api);
console.log('+:Routes added')

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});
// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    console.log(res.locals.error);
    // render the error page
    res.status(err.status || 500);
    if ([401, 403].indexOf(err.status) != -1) {
        res.render('401', {err_description: err.message});
    } else {
        res.render('404', {err_description: err.message});
    }
});

createConnections();

module.exports = app;



