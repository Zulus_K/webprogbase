const Publication = require('@models').Publication;
const db = require('@DB/controller');

//create new entity
exports.create = function (title, author_id, image_id, tags, difficult, description, text) {
    return db.create(Publication,{
        title: title,
        author_id: author_id,
        image_id: image_id,
        difficult: difficult,
        tags: tags,
        description: description,
        text: text
    });
}

exports.getAll = function () {
    return db.getAll(Publication);
}

exports.getById = function (id) {
    console.log(id)
    return db.getById(Publication, id);
}

exports.size = function () {
    return db.size(Publication);
}

exports.remove = function (id) {
    return db.remove(Publication, id);
}

exports.find = function (query, page, limit, sort) {
    return db.find(Publication, query, page, limit, sort);
}