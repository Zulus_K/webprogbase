const User = require('@models').User;
const controller = require('@DB/controller');

//create new entity
exports.create = async function (fullname, telegram, email, password, isAdmin) {
    return controller.create(User, {
        fullname: fullname,
        telegram: telegram,
        email: email,
        isAdmin: isAdmin,
        password: password,
    });
}

exports.getAll = function () {
    return controller.getAll(User);
}

exports.getById = function (id) {
    return controller.getById(User, id);
}

exports.size = function () {
    return controller.size(User);
}

exports.remove = function (id) {
    return controller.remove(User, id);
}

exports.findByEmail = function (value) {
    return controller.findOne(User, {email: value});
}
exports.find = function (query, page, limit, sort) {
    return controller.find(User, query, page, limit, sort);
}