const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Utils = require('@utils');
const config = require('@config');

const UserSchema = new Schema({
    fullname: {
        type: String,
        default: "No name", required: true
    },
    telegram: {
        type: String, default: ""
    },
    password: {
        type: String, required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    isAdmin: {
        type: Boolean,
        default: false,
        required: true
    },
    publications: {
        type: [Schema.Types.ObjectId],
        default: []
    },
    tokens: {
        access: String,
        refresh: String
    }
});
UserSchema.plugin(require('mongoose-paginate'));

UserSchema.index({email: 1}, {unique: true})
// convert password to hash
UserSchema.pre('save', async function (next) {
    if (this.isModified('password') || this.isNew) {
        try {
            this.password = await Utils.crypto.hash(this.password);
            this.tokens.access="";
            this.tokens.refresh="";
        } catch (err) {
            return next(err);
        }
    }
    next();
});

// check is passwords are equals
UserSchema.methods.comparePassword = function (password) {
    return Utils.crypto.compare(password, this.password);
};

// add publication to list
UserSchema.methods.addPublication = function (publication) {
    if (publication && this.publications.indexOf(publication) < 0) {
        this.publications.push(publication);
        return this.save();
    }
};

// remvoe publication from list
UserSchema.methods.removePublication = function (publciation) {
    if (publciation && this.publications.indexOf(publciation) >= 0) {
        this.publications.splice(this.publications.indexOf(publciation), 1);
        return this.save();
    }

}

// check tokens in user
UserSchema.methods.checkToken = function (name, token) {
    return this.tokens[name] === token;
};
// set tokens in user
UserSchema.methods.setTokens = function (tokens, name) {
    if (name) {
        this.tokens[name] = tokens;
    } else {
        this.tokens = tokens;
    }
    return this.save();
};

// update fields
UserSchema.methods.update = function (values) {
    for (field in values) {
        if (!config.validate('user', field, values[field])) {
            throw {code: config.errors.INVALID_VALUE, message: `Invalid value ${field}:'${values[field]}'`}
        }
    }
    if (values.fullname) {
        this.fullname = values.fullname;
    }
    if (values.email) {
        this.email = values.email;
    }
    if (values.telegram) {
        this.telegram = values.telegram;
    }
    if (values.isAdmin) {
        this.isAdmin = values.isAdmin;
    }
    if (values.password) {
        this.password = values.password;
    }
    return this.save();
}

module.exports = mongoose.model('User', UserSchema);
