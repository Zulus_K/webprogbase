const User=require('@DB/models/user')
const Publication=require('@DB/models/publication')

module.exports = {
    User: User,
    Publication: Publication
}