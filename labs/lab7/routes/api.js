"use strict";
const express = require('express');
let router = express.Router();
const Utils = require('@utils');
const config=require('@config');

router.use('/v1/auth', require('@APIv1/auth'))
router.use('/v1/users', require('@APIv1/users'))
router.use('/v1/publications', require('@APIv1/publications'))
router.use('/v1/res', require('@APIv1/res'))

router.get('/v1/', async (req, res, next) => {
    let args = {
        APIroad: Utils.md(await Utils.readFile(config.APIroad))
    };
    res.render('api', args);
})
module.exports = router;