"use strict";
let router = require('express').Router();
const DBpublications = require("@DB/controller.publications");
const utils = require("@utils");

/* GET home page. */
router.get('/', async function (req, res, next) {
    try {
        let args = await utils.getBasicArgsEJS(req);
        args.number_of_publications = await DBpublications.size();
        // console.log(args);
        res.render('index', args);
    } catch (e) {
        console.log(e);
        throw e;
    }
});
module.exports = router;
