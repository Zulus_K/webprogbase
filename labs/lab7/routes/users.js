"use strict";
let router = require('express').Router();
const authorization = require('../utils/auth');
const DBuser = require('@DB/controller.users');
const DBpublications = require('@DB/controller.publications');
const utils = require('@utils');


router.get('/', authorization.local.checkAuth, async (req, res) => {
    if (!req.query.page) {
        req.query.page = 1;
    }
    try {
        let args = await utils.getBasicArgsEJS(req);
        let result=await utils.findPublicationByAuthorId(req);
        args=utils.convertPagination(result,args);
        args.startURL = "/users/?";
        args.searchName = (req.query.title) ? req.query.title : "";
        res.render('profile', args);
    } catch (e) {
        console.log(e);
        next();
    }
});

router.get('/admin', authorization.checkAdmin, async (req, res, next) => {
    if (!req.query.page) {
        req.query.page = 1;
    }
    try {
        let args = await utils.getBasicArgsEJS(req);
        args=utils.convertPagination(await utils.getAllUsers(req.query),argls);
        args.startURL = "/users/admin?";
        console.log(`+ get page admin=${req.query.page}`);
        res.render('admin', args);
    } catch (e) {
        console.log(e);
        next();
    }
});

router.post('/remove/:id', authorization.checkAdmin, async (req, res, next) => {
    try {
        console.log(`try to remove ${req.params.id}`)
        await DBuser.remove(req.params.id);
        res.redirect('/users/admin');
    } catch (e) {
        utils.throwError(next, 500, "Cannot delete user, error");
    }
});
module.exports = router;







