let router = require('express').Router();
const storage_users = require("@DB/controller.users");
const auth = require('@auth');
const utils = require('@utils');

router.get('/registration', async function (req, res, next) {
    try {
        res.render('registration', {});
    } catch (e) {
        utils.throwError(next, 500)
    }
});

router.post('/register', async function (req, res, next) {
    try {
        if (!req.body.fullname || !req.body.email || !req.body.password) {
            console.log('-not full user refgistration info');
            res.redirect('/auth/registration');
        }
        await storage_users.create(
            req.body.fullname,
            req.body.telegram,
            req.body.email,
            req.body.password,
            false);
        console.log(`register +`);
        res.redirect("/");
    } catch (e) {
        console.log(e);
        utils.throwError(next, 401, `This '${req.body.login}' login is already taken`)
    }
})

router.get('/login', async (req, res, next) => {
    res.render('login', {});
})


router.get('/logout', auth.local.checkAuth, (req, res) => {
    req.logout();
    res.redirect('/');
});

router.post('/login', auth.local.authenticate('/users/', '/auth/login/'));

module.exports = router;




