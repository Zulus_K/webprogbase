// user login
// user get tokens by refresh token
// user register
// user logout

const express = require('express');
let router = express.Router();
const auth = require('@auth');
const Utils = require('@utils');
const DBuser = require('@DB/controller.users');
const config = require('@config');
let tools = {};


tools.check = {
    register: (args) => {
        return args.fullname &&
            args.email &&
            args.password;
    },
    login: (args) => {
        return args.email && args.password
    }
};
tools.collect = {
    register: (req) => {
        let args = {};
        args.fullname = req.body.fullname;
        args.password = req.body.password;
        args.email = req.body.email;
        args.telegram = req.body.telegram;
        return args;
    },
    login: (req) => {
        let args = {};
        args.password = req.body.password;
        args.email = req.body.email;
        return args;
    }
};
tools.generateTokens = function (user) {
    // generate tokens
    const tokens = {access: auth.jwt.token('jwt', user), refresh: auth.jwt.token('refresh', user)};
    // add tokens to db
    return user.setTokens(tokens);
}

router.post('/login', auth.local.authorize, async function (req, res, next) {
    console.log('+:login call');
    //check, is local strategy had authorized user
    if (req.user) {
        //generate tokens and save them at storage
        await tools.generateTokens(req.user);
        // send response
        console.log('+:login +')
        return res.json({success: true, tokens: req.user.tokens}).end();
    } else {
        console.log('+:login -')
        //system couldn't authorize user, return error at response
        return Utils.errorAPI(res, 400, '`email` and `password` fields required');
    }
});

router.post('/register', async function (req, res, next) {
    const args = tools.collect.register(req);
    if (!tools.check.register(args)) {
        //some field required
        return Utils.errorAPI(res, 400, "Some fields required");
    } else if (!config.validate('user', 'password', args.password)) {
        Utils.errorAPI(res, 400, 'Please, use only characters A-z, 0-9 in your password, it should contain more than 8 symbols');
    } else if (!config.validate('user', 'email', args.email)) {
        Utils.errorAPI(res, 400, 'Email should be correct');
    } else if (!config.validate('user', 'fullname', args.fullname)) {
        Utils.errorAPI(res, 400, 'Please, use only letters in your name, it should starts from caps letter');
    } else {
        try {
            // create new user
            let user = await DBuser.create(args.fullname, args.telegram, args.email, args.password, false);
            //generate tokens and save them at storage
            await tools.generateTokens(user);
            // send response to client
            res.json({success: true, tokens: user.tokens}).end();
        } catch (e) {
            console.log(`- Register: ${e}`);
            Utils.errorAPI(res, 400, `This email ${req.body.email} is already taken`);
        }
    }
});

router.post('/logout', async function (req, res, next) {
    console.log('+:logout call');
    //check, is local strategy had authorized user
    if (req.user) {
        //generate tokens and save them at storage
        await tools.generateTokens(req.user);
        // send response
        console.log('+:login +')
        return res.json({success: true}).end();
    } else {
        console.log('+:logout -')
        //system couldn't authorize user, return error at response
        return Utils.errorAPI(res, 400, "Credentials required");
    }
});

router.get('/token', auth.jwt.check.refresh, async function (req, res, next) {
    await req.user.setTokens(auth.jwt.token('jwt', req.user), 'access');
    return res.json({success: true, token: req.user.tokens.access}).end();
})
module.exports = router;