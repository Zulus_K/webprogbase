// get res by id
// remove res
// create res
"use strict";
const express = require('express');
let router = express.Router();
const Utils = require('@utils');
const DBimage = require('@DB/controller.image')
const config = require('@config');
const auth = require('@auth')
let tools = {
    collect: {
        find: (req) => {
            return {
                query: tools.query.find(req.query)
            }
        },
        delete: (req) => {
            return {
                query: tools.query.delete(req.body)
            }
        },
        post: (req) => {
            return {
                query: tools.query.post(req)
            }
        }
    },
    check: {
        find: (args) => {
            return args.query && args.query._id;
        },
        delete: (args) => {
            return args.query && args.query.id;
        },
        post: (args) => {
            return args.query && args.query.file;
        }
    },
    query: {
        find: (req) => {
            let query = {}
            //id
            if (req.id && Utils.isValidID(req.id)) {
                query._id = Utils.str2id(req.id);
            }
            return query;
        },
        delete: (req) => {
            let query = {};
            if (req.id && Utils.isValidID(req.id)) {
                query.id = Utils.str2id(req.id);
            }
            return query;
        },
        post: (req) => {
            let query = {};
            if (req.files && req.files.file) {
                query.file = req.files.file;
            }
            return query;
        }
    },
    result: {
        delete: (args) => {
            return {
                success: true,
                id: args.query.id,
            }
        },
        post: (args) => {
            return {
                success: true,
                id: args.id,
            }
        }
    },
    minimize: (file) => {
        return {
            filename: file.filename,
            owner: file.metadata.owner,
            ext: file.metadata.ext,
            uploadAt: file.uploadDate
        }
    }
};

router.route('/')
    .get(async (req, res, next) => {
        const args = tools.collect.find(req);
        if (!tools.check.find(args)) {
            return Utils.errorAPI(res, 400, "Invalid arguments");
        }
        try {
            console.log(args)
            const result = await DBimage.getById(args.query._id);
            if (!result) {
                return Utils.errorAPI(res, 404, "Not found");
            }
            res.type(result.metadata.ext || result.filename.split('.').pop());
            (await result.stream).pipe(res);
        } catch (e) {
            console.log(e);
            return Utils.errorAPI(res, 500, "Server error");
        }
    })
    .delete(auth.jwt.check.login, async (req, res, next) => {
        const args = tools.collect.delete(req);
        if (!tools.check.delete(args, req.user)) {
            return Utils.errorAPI(res, 400, "Invalid arguments");
        }
        try {
            args.file = await DBimage.getById(args.query.id, true);
            if (!args.file) {
                return Utils.errorAPI(res, 404, "No such image");
            } else if (String(args.file.metadata.owner) !== String(req.user._id) && !req.user.isAdmin) {
                return Utils.errorAPI(res, 401, "Permission denied");
            }
        } catch (e) {
            return Utils.errorAPI(res, 500, "Server error");
        }
        try {
            await DBimage.remove(args.query.id);
            return res.json(tools.result.delete(args));
        } catch (e) {
            console.log(e);
            return Utils.errorAPI(res, 404, "No such image");
        }
    })
    .post(auth.jwt.check.login, async (req, res, next) => {
        const args = tools.collect.post(req);
        // console.log(JSON.stringify(args));
        if (!tools.check.post(args, req.user)) {
            return Utils.errorAPI(res, 400, "Invalid arguments");
        }
        try {
            args.id = await DBimage.saveImage(
                args.query.file,
                req.user
            );
            if (args.id) {
                return res.json(tools.result.post(args));
            } else {
                return Utils.errorAPI(res, 400, "Bad arguments");
            }
        } catch (e) {
            console.log(e);
            return Utils.errorAPI(res, 500, "Server error");
        }
    })

module.exports = router;