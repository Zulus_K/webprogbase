"use strict";
let router =  require('express').Router();
const DBpublication = require('@DB/controller.publications');
const DBimage = require('@DB/controller.image');
const DBuser = require('@DB/controller.users');
const utils = require('@utils');
const auth = require('@auth');

router.get('/new', auth.local.checkAuth, function (req, res, next) {
    try {
        res.render("newPublication");
    } catch (err) {
        console.log(err);
        return utils.throwError(next, 404);
    }
});

router.get('/:id', async function (req, res, next) {
    try {
        let publication = await  DBpublication.getById(req.params.id);
        if (!publication) throw "No such publication";
        let args = await utils.getBasicArgsEJS(req);
        //get info about publication's
        args.publication = publication;
        args.publication.author = await DBuser.getById(publication.author_id);
        if (!publication.author ) {
            publication.author = {
                fullname: 'DELETED USER',
                _id: ''
            };
        }
        res.render('publication', args);
    } catch (e) {
        console.log(e);
       return utils.throwError(next, 404, e);
    }
});

router.get('/', async function (req, res, next) {
    try {
        let args = await utils.getBasicArgsEJS(req);
        let result=await utils.findPublicationBySearchQuery(req.query);
        args=utils.convertPagination(result,args);
        args.startURL = "/publications?" + ((req.query.title) ? `title=${req.query.title}&` : "");
        args.searchName = (req.query.title) ? req.query.title : "";
        console.log(`+ get page=${req.query.page} & name=${req.query.title}`);
        res.render('publications_list', args);
    } catch (e) {
        console.log(e);
        next();
    }
});

router.post("/add", auth.local.checkAuth, async (req, res, next) => {
    //create new publication
    // console.log(req.files.logo);
    try {
        let image_id;
        //try to save images in request
        try {
            image_id = ('files' in req && 'logo' in req.files) ? await DBimage.saveImage(req.files.logo,req.user) : '0';
            console.log(image_id)
        } catch (e) {
            console.log(e);
            image_id = '0';
        }
        let publication = await
            DBpublication.create(
                req.body.title,
                req.user._id,
                image_id,
                await utils.getTagsFromQuery(req.body),
                req.body.difficult,
                req.body.description,
                req.body.text)
        //add image to array in users
        await req.user.addPublication(publication._id);
        console.log(`+create ${req.query.title}`);
    } catch (e) {
        console.log("-error at create" + e);
        return utils.throwError(next, 500, "Cannot create publication");
    }
    res.redirect("/publications");
});

router.post("/remove/:id", auth.local.checkAuth, async (req, res, next) => {
    try {
        let entity = await DBpublication.getById(req.params.id);
        if(!entity){
            return utils.throwError(next, 400, "There are no publication with such ID");
        }
        if (entity && ((String(req.user._id) === String(entity.author_id)) || req.user.isAdmin)) {
            await DBpublication.remove(entity._id);
            let owner=await DBuser.getById(entity.author_id);
            if(owner){
                await owner.removePublication(entity._id);
            }
            console.log(`+removed ${req.params.id}`)
        } else {
            console.log(`-user.id!= author.id usr:'${req.user._id}' auth:'${entity.author_id}'`)
            utils.throwError(next, 500, "You haven't permission for this action");
        }
    } catch (e) {
        console.log(`-error at remove ${req.params.id}: ${e}`);
        return utils.throwError(next, 500, "smth is bad in removing");
    }
    res.redirect("/publications");
});

module.exports = router;
