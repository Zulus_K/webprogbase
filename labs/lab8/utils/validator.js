module.exports = function (model, field, value) {
    switch (model) {
        case 'user':
            return user(field, value);
            break;
        case 'publication':
            return publication(field, value);
            break;
        default:
            return true;
    }
}
function user (field, value) {
    if (field in user) {
        return user[field](value);
    } else {
        return true;
    }
}

user.password = function (value) {
    return /^[a-z0-9]+$/i.test(value) && value.length >= 1;
}
user.email = function (value) {
    return true
    // return /^[a-z0-9/.]+@[a-z0-9/.]+$/i.test(value);
}
user.fullname = function (value) {
    // return true
    return /^[a-z' ]+$/i.test(value) && value.length > 1;
}

publication.title = function (value) {
    return /^[a-z0-9' ]+$/i.test(value) && value.length > 1;
}

function publication (field, value) {
    if (field in publication) {
        console.log(field,value)
        return publication[field](value);
    } else {
        return true;
    }
}