const DBpublication = require('@DB/controller.publications');
const DBimage = require('@DB/controller.image');
const DBuser = require('@DB/controller.users');
const auth = require('@auth');
const crypto = require('crypto');
const ObjectID = require('mongoose').Types.ObjectId;
const fs=require('fs-promise');
const commonmark=require('commonmark');
exports.findPublicationBySearchQuery = function getDataByQuery (query) {
    if (query.page && Number(query.page) <= 0) {
        throw("Invalid page value")
    } else if (!query.title || query.title === "") {
        return DBpublication.find({}, query.page, Number(process.env.LIMIT), {title: 1});
    } else {
        return DBpublication.find({title: new RegExp(`^${query.title.trim()}`, "i")}, query.page, Number(process.env.LIMIT), {title: 1});
    }
}

exports.findPublicationByAuthorId = function getDataByQuery (req) {
    if (req.query.page && Number(req.query.page) <= 0) {
        throw("Invalid page value")
    } else {
        return DBpublication.find({author_id: req.user._id}, req.query.page, Number(process.env.LIMIT), {title: 1});
    }
}

exports.getAllUsers = async function (query) {
    if (query.page && Number(query.page) <= 0) {
        throw("Invalid page value")
    } else {
        return DBuser.find({}, query.page, Number(process.env.LIMIT), {email: 1});
    }
}

//parse tags from request
exports.getTagsFromQuery = async function parseTags (body) {
    let tags = [];
    for (let i = 0; ("tag" + i) in body; i++) {
        let val = body["tag" + i];
        if (val.length > 0) {
            tags.push(val);
        }
    }
    return tags;
}

exports.getUserInfo = async function (req) {
    let user = {};
    if (auth.local.isAuthorized(req)) {
        user.isAuthorized = true;
        user.info = req.user;
    } else {
        user.isAuthorized = false;
        user.info = {};
    }
    return user;
}

exports.getBasicArgsEJS = async function (req) {
    let args = {
        user: await this.getUserInfo(req),
    };
    return args;
}

exports.throwError = function (next, status, msg) {
    let err = new Error(msg);
    err.status = status;
    next(err);
}

exports.crypto = {
    hash: (data) => {
        const hash = crypto.createHmac('sha512', process.env.SALT);
        hash.update(data);
        const value = hash.digest('hex');
        return value;
    },
    compare: (plainData, hash) => {
        return this.crypto.hash(plainData) === hash;
    }
}

exports.paginateOptions = function (page, limit, sort) {
    return {
        page: (Number(page) && Number(page) >= 0) ? page : 1,
        limit: (Number(limit) && Number(limit) >= 0) ? limit : 20,
        sort: sort || {}
    };
}

exports.isValidID = function (id) {
    return ObjectID.isValid(id);
}
exports.str2id=function (str) {
    if(typeof str !='string') str;
    try {
        return new ObjectID.createFromHexString(str);
    }catch (e){
        return new ObjectID();
    }
}

exports.parseJSON=function (str) {
    try{
        return JSON.parse(str);
    }catch (e){
    }
}

exports.convertPagination = function (result, args) {
    args.currentPage = result.page;
    args.range = Number(process.env.RANGE);
    args.items = result.docs;
    console.log(result)
    args.pageCount = result.pages;
    return args;
}

exports.errorAPI = (res, code, message) => {
    return res
        .status(code)
        .json({success: false, message: message})
        .end();
}

exports.md=function (text) {
    var reader = new commonmark.Parser();
    var writer = new commonmark.HtmlRenderer();
    var parsed = reader.parse(text); // parsed is a 'Node' tree
// transform parsed if you like...
    return writer.render(parsed); // result is a String
}

exports.readFile=function (path) {
    return fs.readFile(path,'utf8');
}