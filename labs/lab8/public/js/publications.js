let Model = {
    items: [],
    page: Number(Tools.getFromURL('page', window.location.href) || 1),
    limit: Number(Tools.getFromURL('limit', window.location.href) || 8),
    // page: 1,
    // limit: 8,
    range: 3,
    filter: "",
    author_id: Tools.getFromURL('author', window.location.href)
}

function removePublications () {
    Model.items = [];
    let e = document.getElementById('publications-grid');
    while (e.hasChildNodes()) {
        e.removeChild(e.lastChild);
    }
}

function addPlaceholders (nodes) {
    for (var i = 0; i < nodes.length; i++) {
        nodes[i].src = `/api/v1/res?id=${nodes[i].id}`;
        nodes[i].addEventListener('error', (event) => usePlaceholder(event.srcElement));
    }
}

function getTags (arr) {
    let tags = [];
    arr.forEach((value) => {
        tags.push(value.substr(0, 10))
    })
    return tags;
}

function collect (items) {
    let args = {items: []};
    items.forEach((item) => {
        args.items.push({
            id: item.id,
            title: item.title.substr(0, 20),
            description: item.description.substr(0, 100),
            difficult: item.difficult,
            tags: getTags(item.tags),
            author_id: item.author_id,
            image_id: item.image_id,
            middle: item.difficult < 7 && item.difficult > 3,
            easy: item.difficult <= 3,
            hard: item.difficult >= 7
        })
    });
    return args;
}

function addNewPublications (items) {
    let e = document.getElementById('publications-grid');
    if (items.length > 0) {
        e.innerHTML = Renderer.render(collect(items), 'card');
        addPlaceholders(e.getElementsByClassName('publication-prev-img'))
        addAuthors();
    } else {
        console.log('1')
        e.innerHTML = Renderer.render({message: `No publication found`}, 'card-error');
    }
}

function addAuthors () {
    let cards = Array.prototype.slice.call(document.getElementsByClassName('publication-card-item'), 0);
    cards.forEach((card) => {
        const author = card.id.split(':')[1];
        let avatar = card.getElementsByClassName('author-avatar')[0];
        let name = card.getElementsByClassName('author-name')[0];
        let link = card.getElementsByClassName('author-link')[0];
        API.getUser(`id=${author}`, (response) => {
            avatar.src = Tools.gravatar(response.items[0].email, 64);
            name.innerHTML = Renderer.secure(response.items[0].fullname);
            link.innerHTML = '@' + Renderer.secure(response.items[0].telegram);
        })
    })
}

function gotoPage (page) {
    if (page < 1 || page > Model.pages || page == Model.page) {
        return;
    } else {
        Model.page = page;
        loadPublications();
    }
}

function paginate (response) {
    Model.pages = response.pages;
    let container = document.getElementById('pagination');
    Pagination.create(container, Model, gotoPage)
}

function loadPublications () {
    removePublications();
    let query = `page=${Model.page}&limit=${Model.limit}`;
    if (Model.filter && Model.filter.length > 0) {
        query += `&title=${Model.filter}`;
    }
    if (Model.author_id) {
        query += `&author=${Model.author_id}`;
    }
    API.getPublications(query,
        (response) => {
            paginate(response);
            addNewPublications(response.items)
        },
        (response) => {
            console.log(response)
        })
}

function find () {
    Model.filter = document.getElementById('search').value;
    loadPublications();
}

document.addEventListener('DOMContentLoaded', (event) => {
    Renderer.config('card');
    // Renderer.config('card-tags');
    Renderer.config('pagination-prev');
    Renderer.config('pagination-next');
    Renderer.config('pagination-item');
    Renderer.config('card-error');
    // Renderer.config('pagination-spacer');
    loadPublications();
    //create tree
    //make pagination
    //redraw
})