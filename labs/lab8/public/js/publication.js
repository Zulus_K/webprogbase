Model = {
    publication: {}
}

function savePublication (publication) {
    Model.publication = publication;
    if (APP.isUserLogged() && (publication.author_id == localStorage.getItem('id') || APP.isAdmin())) {
        Model.publication.removaible = true;
    }
    if (Model.publication.difficult <= 3) {
        Model.publication.easy = true;
    } else if (Model.publication.difficult <= 7) {
        Model.publication.medium = true;
    } else {
        Model.publication.hard = true;
    }
}

function loadAvatar () {
    let avatars = document.getElementsByClassName('author-avatar');
    for (let i = 0; i < avatars.length; i++) {
        avatars[i].src = Tools.gravatar(Model.publication.author.email, 256);
        avatars[i].addEventListener('error', (event) => usePlaceholder(event.srcElement));
    }
}

function remove () {
    Tools.closeModal('modal-remove');
    API.removePublication(Model.publication.id, (response) => {
        window.location.href = '/publications'
    }, (response, status) => {
        //need to login
        if (status == 401) {
            window.open('/auth/login', '_blank');
        } else {
            showModal_forbidden();
        }
    });
}

function showModal_forbidden () {
    //add modal window
    if (!document.getElementById('modal-forbidden')) {
        document.getElementsByTagName('body')[0].appendChild(Tools.parseHTML(Renderer.render({
            text: `Dear ${localStorage.getItem('fullname')}, You don't have enough rights to remove the post`,
            title: 'Important',
            style: 'is-warning',
            text_button_2: 'OK',
            onButton2: "Tools.closeModal('modal-forbidden')",
            id: 'modal-forbidden'
        }, 'modal')))
    }
    document.getElementById('modal-forbidden').classList.add('is-active');
}

function showModal () {
    //add modal window
    if (!document.getElementById('modal-remove')) {
        document.getElementsByTagName('body')[0].appendChild(Tools.parseHTML(Renderer.render({
            text: 'Are you sure you want to delete this post?',
            title: 'Alert',
            style: 'is-warning',
            style_button_1: 'is-danger',
            text_button_1: 'Delete',
            text_button_2: 'Cancel',
            onButton1: 'remove()',
            onButton2: "Tools.closeModal('modal-remove')",
            id: 'modal-remove'
        }, 'modal')))
    }
    document.getElementById('modal-remove').classList.add('is-active');
}

function update () {
    document.getElementById('publication-container').appendChild(Tools.parseHTML(Renderer.render(Model.publication, 'publication-view')));
    document.getElementById('page-title').innerHTML = Renderer.secure(Model.publication.title);
    document.getElementById('page-description').setAttribute('content', Renderer.secure(Model.publication.description));

    loadAvatar();
    if (localStorage.getItem('id') == Model.publication.author.id) {
        //show button remove
    }
}

function leave (response) {
    if (response.status <500) {
        window.location.href = '/404';
    } else {
        window.location.href = '/500';
    }
}

function loadUser (response) {
    API.getUser(`id=${Model.publication.author_id}`, (response) => {
        if (response.items && response.items[0]) {
            Model.publication.author = response.items[0];
            update();
        } else {
            Model.publication.author = getIdleUser();
        }
    }, leave)
}

function loadPublication () {
    const id = window.location.href.split('/').pop();
    API.getPublications(`id=${id}`, (response) => {
        if (response.items && response.items.length == 1) {
            savePublication(response.items[0]);
            loadUser();
        } else {
            window.location.href = '/404';
        }
    }, leave);
}

document.addEventListener('DOMContentLoaded', (event) => {
    Renderer.config('publication-view');
    Renderer.config('modal');
    loadPublication();
})