const fields = ['email', 'password'];

function bindCancel () {
    let e = document.getElementById('cancel');
    e.addEventListener('click', function (e) {
        history.back();
    })
}

function login (response) {
    console.log(response);
    APP.setCredentials(response);
    APP.setUser(response.user);
    console.log('refresh', localStorage.getItem('token_refresh'));
    console.log('access', localStorage.getItem('token_access'));
    window.location.href = '/';
}

function bindSubmit () {
    document.getElementById('submit').addEventListener('click', (event) => {
        let credentials = {};
        //check is all fields are valid
        for (var field of fields) {
            const e = document.getElementById(field);
            if (e && e.getAttribute('valid')) {
                credentials[field] = e.value;
            } else {
                return;
            }
        }
        console.log(credentials)
        //create req to API
        API.login(credentials, login, (response) => {
            showModal_noSuchUser(response);
        });
    })
}

function showModal_noSuchUser (response) {
    //add modal window
    if (!document.getElementById('modal-noSuchUser')) {
        document.getElementsByTagName('body')[0].appendChild(Tools.parseHTML(Renderer.render({
            text: response.message,
            title: 'Important',
            style: 'is-warning',
            text_button_2: 'OK',
            onButton2: "Tools.closeModal('modal-noSuchUser')",
            id: 'modal-noSuchUser'
        }, 'modal')))
    }
    document.getElementById('modal-noSuchUser').classList.add('is-active');
    document.getElementById('clear').click()
}


function bindInputs () {
    fields.forEach(field => APP.bindValidation(field, APP.validate, 'login'))
    bindSubmit();
    bindCancel();
}

document.addEventListener('DOMContentLoaded', function () {
    Renderer.config('modal')
    //check, if user is logged, redirect
    if (APP.isUserLogged()) {
        window.location.href = "/";
        return;
    }
    bindInputs();
})

