const fields = ['email', 'telegram', 'fullname', 'password'];


function bindCancel () {
    let e = document.getElementById('cancel');
    e.addEventListener('click', function (e) {
        history.back();
    })
}

function register (response) {
    console.log(response);
    APP.setCredentials(response);
    APP.setUser(response.user);
    console.log('refresh', localStorage.getItem('token_refresh'))
    console.log('access', localStorage.getItem('token_access'))
    window.location.href='/';
}

function bindSubmit () {
    document.getElementById('submit').addEventListener('click', (event) => {
        let credentials = {};
        //check is all fields are valid
        for (var field of fields) {
            const e = document.getElementById(field);
            if (e && e.getAttribute('valid')) {
                credentials[field] = e.value;
            } else {
                alert(field);
                return;
            }
        }
        console.log(credentials)
        //create req to API
        API.register(credentials, register, (response) => alert(response.message));
    })
}

function bindInputs () {
    fields.forEach((field) => APP.bindValidation(field,APP.validate,'registration'));
    bindSubmit();
    bindCancel();
}

// start
document.addEventListener('DOMContentLoaded', function () {
    //check, if user is logged, redirect
    if (APP.isUserLogged()) {
        window.location.href = "/";
        return;
    }
    bindInputs();
})
