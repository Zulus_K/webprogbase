const fields = ['title', 'difficult', 'text', 'description'];
let tags = []

function addTag (text) {
    if (tags.indexOf(text) < 0) {
        text = text.substr(0, 10);
        tags.push(text)
        document.getElementById('tags-container').appendChild(Tools.parseHTML(Renderer.render({text: text}, 'create-tag')));
    }
}

function removeTag (text) {
    if (tags.indexOf(text) >= 0) {
        document.getElementById(`tags-container`).removeChild(document.getElementById(`$!_${text}`));
        tags.splice(tags.indexOf(text, 1));
    }
}

function bindCancel () {
    let e = document.getElementById('cancel');
    e.addEventListener('click', function (e) {
        history.back();
    })
}

function create (response) {
    window.location.href = '/publications';
}

function removeSelectedFile () {
    var file = document.getElementById("file");
    if (file.files.length > 0) {
        file.value = "";
        document.getElementById('filename').innerHTML = ""
        document.getElementById('filename').classList.add('is-hidden');
        document.getElementById('remove-file').classList.add('is-hidden');
    }
}

function showSelectedFile () {
    var file = document.getElementById("file");
    if (file.files.length > 0) {
        document.getElementById('filename').innerHTML = file.files[0].name;
        document.getElementById('filename').classList.remove('is-hidden');
        document.getElementById('remove-file').classList.remove('is-hidden');
    }
}

function removeModal (id) {
    Tools.closeModal(id);
    document.getElementsByTagName('body')[0].removeChild(document.getElementById(id));
}

function showModal_notAllFieldsAreValid (field) {
    //add modal window
    const id = 'modal-notAllFieldsAreValid';
    if (!document.getElementById(id)) {
        document.getElementsByTagName('body')[0].appendChild(Tools.parseHTML(Renderer.render({
            text: `Not all fields are valid: '${field}'`,
            title: 'Important',
            style: 'is-warning',
            text_button_2: 'OK',
            onButton2: `removeModal('${id}')`,
            id: id
        }, 'modal')))
    }
    document.getElementById(id).classList.add('is-active');
}

function showModal_error (respone) {
    const id = 'modal-error';
    //add modal window
    if (!document.getElementById(id)) {
        document.getElementsByTagName('body')[0].appendChild(Tools.parseHTML(Renderer.render({
            text: respone.message,
            title: 'Ooops',
            style: 'is-danger',
            style_button_2: 'is-warning',
            text_button_2: 'OK',
            onButton2: `removeModal('${id}')`,
            id: id
        }, 'modal')))
    }
    document.getElementById(id).classList.add('is-active');
}

function bindSubmit () {
    document.getElementById('submit').addEventListener('click', async (event) => {
        let args = {};
        //check is all fields are valid
        for (var field of fields) {
            const e = document.getElementById(field);
            console.log(field, e, e.getAttribute('valid'), Boolean(e.getAttribute('valid')))
            if (e && Boolean(e.getAttribute('valid'))) {
                args[field] = e.value || e.innerHTML;
            } else {
                showModal_notAllFieldsAreValid(field);
                return;
            }
        }
        args.tags = tags;
        //try to save file
        try {
            const file = document.getElementById('file').files[0];
            if (file) {
                let response = await API.saveFile(file)
                response = await response.json();
                console.log(response)
                if (response.success) {
                    args.image_id = response.id;
                } else {
                    args.image_id = '0';
                }
            }
        } catch (e) {
            console.log(e)
        }
        try {
            let res = await API.createPublication(args);
            response = await res.json();
            if (response.success) {
                window.location.href = `
        /publications/${response.publication.id}`;
                return;
            } else if (args.image_id) {
                //remove uploaded file
                let a = await API.removeFile({id: args.image_id});
                a = await a.json();
                if (a.success) {
                    console.log('removed file ' + args.image_id)
                }
            }
            if (res.status == 401 || res.status == 403) {
                APP.setUser()
                APP.setCredentials()
                return window.location.href = '/auth/login';
            }
            showModal_error(response);
        } catch (e) {
            console.log(e)
        }
    })
}

function bindTags () {
    const node = document.getElementById('tags-input');
    node.addEventListener('keydown', function onEvent (event) {
        if (event.key === "Enter") {
            if (node.value.length > 1) {
                addTag(node.value);
                node.value = "";
            }
        }
    });
}

function bindInputs () {
    fields.forEach((field) => APP.bindValidation(field, APP.validate, 'publication'));
    bindSubmit();
    bindCancel();
    bindTags();
}

document.addEventListener('DOMContentLoaded', function () {

    if (!APP.isUserLogged()) {
        window.location.href = "/";
        return;
    }
    Renderer.config('modal');
    Renderer.config('create-tag');
    bindInputs();
})