const API = {
    register: function (credentials, callbackSuccess, callbackFail) {
        var xhr = new XMLHttpRequest();
        xhr.open('post', `/api/v1/auth/register`, true);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.onreadystatechange = function () {
            if (xhr.readyState != 4) return;
            try {
                const response = JSON.parse(xhr.responseText);
                if (response.success) {
                    callbackSuccess(response);
                } else {
                    callbackFail(response);
                }
            } catch (e) {
                console.log(e)
            }
        }
        xhr.send(JSON.stringify(credentials))
    },
    login: function (credentials, callbackSuccess, callbackFail) {
        var xhr = new XMLHttpRequest();
        xhr.open('post', `/api/v1/auth/login`, true);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.onreadystatechange = function () {
            if (xhr.readyState != 4) return;
            try {
                const response = JSON.parse(xhr.responseText);
                if (response.success) {
                    callbackSuccess(response);
                } else {
                    callbackFail(response);
                }
            } catch (e) {
                console.log(e)
            }
        }
        xhr.send(JSON.stringify(credentials))
    },
    logout: function (callbackSuccess, callbackFail) {
        var xhr = new XMLHttpRequest();
        xhr.open('post', `/api/v1/auth/logout`, true);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token_access'));
        xhr.onreadystatechange = function () {
            if (xhr.readyState != 4) return;
            try {
                const response = JSON.parse(xhr.responseText);
                if (response.success) {
                    callbackSuccess(response);
                } else {
                    callbackFail(response);
                }
            } catch (e) {
                console.log(e)
            }
        }
        xhr.send();
    },
    getUser: function (args, callbackSuccess, callbackFail) {
        var xhr = new XMLHttpRequest();
        xhr.open('get', `/api/v1/users?` + args, true);
        xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token_access'));
        xhr.onreadystatechange = function () {
            if (xhr.readyState != 4) return;
            try {
                const response = JSON.parse(xhr.responseText);
                if (response.success) {
                    callbackSuccess(response);
                } else {
                    callbackFail(response);
                }
            } catch (e) {
                console.log(e)
            }
        }
        xhr.send();
    },
    getPublications: function (args, callbackSuccess, callbackFail) {
        var xhr = new XMLHttpRequest();
        xhr.open('get', `/api/v1/publications?` + args, true);
        xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token_access'));
        xhr.onreadystatechange = function () {
            if (xhr.readyState != 4) return;
            try {
                const response = JSON.parse(xhr.responseText);
                if (response.success) {
                    callbackSuccess(response);
                } else {
                    callbackFail(response);
                }
            } catch (e) {
                console.log(e)
            }
        }
        xhr.send();
    },
    removePublication: function (id, callbackSuccess, callbackFail) {
        console.log('call remove');
        var xhr = new XMLHttpRequest();
        xhr.open('DELETE', `/api/v1/publications`, true);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token_access'));
        xhr.onreadystatechange = function () {
            if (xhr.readyState != 4) return;
            try {
                const response = JSON.parse(xhr.responseText);
                if (response.success) {
                    callbackSuccess(response);
                } else {
                    //refresh tokens and try again
                    if (xhr.status == 401 || response.message == 'TokenExpiredError') {
                        API.refresh(
                            API.removePublication(id, callbackSuccess, callbackFail),
                            callbackFail)
                    } else {
                        callbackFail(response, xhr.status);
                    }
                }
            } catch (e) {
                console.log(e)
            }
        }
        xhr.send(JSON.stringify({id: id}))
    },
    refresh: function (callbackSuccess, callbackFail) {
        console.log('call refresh');
        var xhr = new XMLHttpRequest();
        xhr.open('GET', `/api/v1/auth/token`, true);
        xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token_refresh'));
        xhr.onreadystatechange = function () {
            if (xhr.readyState != 4) return;
            try {
                const response = JSON.parse(xhr.responseText);
                if (response.success) {
                    localStorage.setItem('token_access', response.token)
                    callbackSuccess(response);
                } else {
                    APP.setCredentials();
                    callbackFail(response, xhr.status);
                }
            } catch (e) {
                console.log(e)
            }
        }
        xhr.send();
    },
    saveFile: function (file) {
        console.log('call save file');
        let data = new FormData();
        data.append('file', file);
        return fetch('/api/v1/res/', {
            method: 'post',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token_access')}`,
            },
            body: data
        })
    },
    removeFile: function (body) {
        let data = new FormData();
        for (key in body) {
            data.append(key, body[key]);
        }
        return fetch('/api/v1/res/', {
            method: 'delete',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token_access')}`,
            },
            body: data
        })
    },
    createPublication: function (body) {
        console.log('call create publication', body);
        console.log('call save file');
        let data = new FormData();
        for (key in body) {
            if (Array.isArray(body[key])) {
                data.append(key, JSON.stringify(body[key]));
            } else {
                data.append(key, body[key]);
            }
        }
        console.log(data)
        return fetch('/api/v1/publications/', {
            method: 'post',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token_access')}`,
            },
            body: data
        })
    }
}

const APP = {
    setCredentials: function (body) {
        if (body && body.tokens && body.tokens.access && body.tokens.refresh) {
            localStorage.setItem('token_refresh', body.tokens.refresh);
            localStorage.setItem('token_access', body.tokens.access);
            localStorage.setItem('isLogged', true);
            document.dispatchEvent(new Event('login'))
        } else {
            localStorage.setItem('token_refresh', "");
            localStorage.setItem('token_access', "");
            localStorage.setItem('isLogged', false);
            APP.setUser();
            document.dispatchEvent(new Event('logout'))
        }
    },

    setUser: function (user) {
        if (user) {
            localStorage.setItem('fullname', user.fullname);
            localStorage.setItem('email', user.email);
            localStorage.setItem('telegram', user.telegram);
            localStorage.setItem('id', user.id);
            localStorage.setItem('isAdmin', user.isAdmin);
        } else {
            localStorage.setItem('isAdmin', "");
            localStorage.setItem('fullname', "");
            localStorage.setItem('email', "");
            localStorage.setItem('telegram', "");
            localStorage.setItem('id', "");
        }
    },
    isAdmin: function () {
        return localStorage.getItem('isAdmin') == 'true';
    },
    isUserLogged: function () {
        console.log(localStorage.getItem('isLogged'))
        return localStorage.getItem('isLogged') == 'true';
    },

    hide: function (id) {
        document.getElementById(id).style.display = "none";
    },

    show: function (id) {
        document.getElementById(id).style.display = "block";
    },

    config: function () {
        // Get all "navbar-burger" elements
        var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

        // Check if there are any navbar burgers
        if ($navbarBurgers.length > 0) {

            // Add a click event on each of them
            $navbarBurgers.forEach(function ($el) {
                $el.addEventListener('click', function () {

                    // Get the target from the "data-target" attribute
                    var target = $el.dataset.target;
                    var $target = document.getElementById(target);

                    // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                    $el.classList.toggle('is-active');
                    $target.classList.toggle('is-active');

                });
            });
        }
    },

    validate: function (id, response) {
        let res = {};
        try {
            res = JSON.parse(response);
        } catch (e) {
            console.log(e)
        }

        let e = document.getElementById(id);
        let h = document.getElementById(`help-${id}`);
        if (!h) {
            h = document.createElement('p');
            document.getElementById(`${id}-field`).appendChild(h);
            h.setAttribute('id', `help-${id}`);
            h.classList.add('help');
        }
        if (res.success && res.result) {
            console.log('+', id, e.getAttribute('valid'));
            e.setAttribute('valid', true);
            e.setCustomValidity("");
            h.innerHTML = ''
            h.classList.add('is-success');
            h.classList.remove('is-danger');
            e.classList.add('is-success');
            e.classList.remove('is-danger');
        } else {
            console.log('-', id)
            e.setCustomValidity(res.message);
            e.setAttribute('valid', false);
            h.innerHTML = (res.success) ? res.message : 'Required';
            h.classList.remove('is-success');
            h.classList.add('is-danger');
            e.classList.remove('is-success');
            e.classList.add('is-danger');
        }
    },

    bindValidation: function (id, validator, path) {
        let e = document.getElementById(id);
        e.addEventListener('input', function (event) {
            var xhr = new XMLHttpRequest();
            xhr.open('post', `/api/v1/validate/${path}/${id}`, true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xhr.onreadystatechange = function () {
                if (xhr.readyState != 4) return;
                validator(id, xhr.responseText);
            }
            xhr.send(JSON.stringify({value: event.target.value || event.target.innerHTML}))
        })
    },

    getAvatar: function () {
        if (!APP.isUserLogged()) return;
        localStorage.setItem('gravatar', Tools.gravatar(localStorage.getItem('email'), 128))
        console.log(localStorage.getItem('gravatar'))
        document.getElementById('header-avatar').setAttribute('src', localStorage.getItem('gravatar'))
    },

    removeAvatar () {
        localStorage.setItem('gravatar', "");
        document.getElementById('header-avatar').setAttribute('src', 'mmm')
    },
    addLink: function (id, url) {
        document.getElementById(id).href = url;
    },
    removeLink: function (id) {
        document.getElementById(id).href = null;
    }
}

const Renderer = {
    templates: [],
    config: function (name) {
        this.templates[name] = document.getElementById(`template-${name}`).innerHTML;
        Mustache.parse(this.templates[name]);
    },
    render: function (args, name) {
        return Mustache.render(this.templates[name], args);
    },
    secure: function (template) {
        return Mustache.render(template, {});
    }
}

const Pagination = {
    createList: function () {
        let list = document.createElement('ul');
        list.id = "pagination-list";
        list.classList.add('pagination-list');
        return list;
    },
    createItem: function (template, callback, page) {
        let div = document.createElement('div');
        let item = Tools.parseHTML(template);
        item.addEventListener('click', () => callback(page))
        return item;
    },
    createPaginationList: function (page, total, range, callback) {
        let list = this.createList();
        //draw pagination
        const left = Math.max(1, page - range);
        const right = Math.min(total, page + range);
        for (let i = left; i <= right; i++) {
            const child = this.createItem(Renderer.render({
                page: i,
                current: i == page
            }, 'pagination-item'), callback, i);
            list.appendChild(child);
        }
        return list;
    },
    create: function (root, Model, callback) {
        this.clear(root);
        if (Model.pages > 1) {
            if (Model.page > 1) {
                root.appendChild(this.createItem(Renderer.render({current: Model.page == 1}, 'pagination-prev'), callback, Model.page - 1));
            }
            if (Model.page < Model.pages) {
                root.appendChild(this.createItem(Renderer.render({current: (Model.page - Model.pages == 0)}, 'pagination-next'), callback, Model.page + 1));
            }
        }
        root.appendChild(this.createPaginationList(Model.page, Model.pages, Model.range, callback))

    },
    clear: function (root) {
        while (root.hasChildNodes()) {
            root.removeChild(root.firstChild)
        }
    }
}

function loginHandler () {
    APP.getAvatar();
    APP.hide('header-login-btn');
    APP.hide('header-register-btn');
    APP.show('header-logout-btn');
    APP.show('header-avatar');
    APP.show('header-avatar-container');
    APP.addLink('header-avatar-container', `/publications?author=${localStorage.getItem('id')}`);

    if (APP.isAdmin()) {
        APP.show('header-isAdmin');
        APP.hide('header-isUser');
    } else {
        APP.hide('header-isAdmin');
        APP.show('header-isUser');
    }
    APP.show('header-add-publication');
}

function logoutHandler () {
    APP.removeAvatar();
    APP.show('header-login-btn');
    APP.show('header-register-btn');
    APP.hide('header-logout-btn');
    APP.hide('header-avatar');
    APP.hide('header-avatar-container');
    APP.hide('header-isAdmin');
    APP.hide('header-isUser');
    APP.hide('header-add-publication');
    APP.removeLink('header-avatar-container');
}

document.addEventListener('DOMContentLoaded', (event) => {
    //process navbar burger
    APP.config();
    if (APP.isUserLogged()) {
        loginHandler();
    } else {
        logoutHandler();
    }
    document.addEventListener('login', loginHandler)
    document.addEventListener('logout', logoutHandler)
    document.getElementById('header-logout-btn').addEventListener('click', () => {
        APP.setCredentials();
        APP.setUser();
    })
})


const Tools = {
    gravatar: function (email, size) {
        email = email.toLowerCase();
        // MD5 (Message-Digest Algorithm) by WebToolkit
        //

        var MD5 = function (s) {
            function L (k, d) {
                return (k << d) | (k >>> (32 - d))
            }

            function K (G, k) {
                var I, d, F, H, x;
                F = (G & 2147483648);
                H = (k & 2147483648);
                I = (G & 1073741824);
                d = (k & 1073741824);
                x = (G & 1073741823) + (k & 1073741823);
                if (I & d) {
                    return (x ^ 2147483648 ^ F ^ H)
                }
                if (I | d) {
                    if (x & 1073741824) {
                        return (x ^ 3221225472 ^ F ^ H)
                    } else {
                        return (x ^ 1073741824 ^ F ^ H)
                    }
                } else {
                    return (x ^ F ^ H)
                }
            }

            function r (d, F, k) {
                return (d & F) | ((~d) & k)
            }

            function q (d, F, k) {
                return (d & k) | (F & (~k))
            }

            function p (d, F, k) {
                return (d ^ F ^ k)
            }

            function n (d, F, k) {
                return (F ^ (d | (~k)))
            }

            function u (G, F, aa, Z, k, H, I) {
                G = K(G, K(K(r(F, aa, Z), k), I));
                return K(L(G, H), F)
            }

            function f (G, F, aa, Z, k, H, I) {
                G = K(G, K(K(q(F, aa, Z), k), I));
                return K(L(G, H), F)
            }

            function D (G, F, aa, Z, k, H, I) {
                G = K(G, K(K(p(F, aa, Z), k), I));
                return K(L(G, H), F)
            }

            function t (G, F, aa, Z, k, H, I) {
                G = K(G, K(K(n(F, aa, Z), k), I));
                return K(L(G, H), F)
            }

            function e (G) {
                var Z;
                var F = G.length;
                var x = F + 8;
                var k = (x - (x % 64)) / 64;
                var I = (k + 1) * 16;
                var aa = Array(I - 1);
                var d = 0;
                var H = 0;
                while (H < F) {
                    Z = (H - (H % 4)) / 4;
                    d = (H % 4) * 8;
                    aa[Z] = (aa[Z] | (G.charCodeAt(H) << d));
                    H++
                }
                Z = (H - (H % 4)) / 4;
                d = (H % 4) * 8;
                aa[Z] = aa[Z] | (128 << d);
                aa[I - 2] = F << 3;
                aa[I - 1] = F >>> 29;
                return aa
            }

            function B (x) {
                var k = "", F = "", G, d;
                for (d = 0; d <= 3; d++) {
                    G = (x >>> (d * 8)) & 255;
                    F = "0" + G.toString(16);
                    k = k + F.substr(F.length - 2, 2)
                }
                return k
            }

            function J (k) {
                k = k.replace(/rn/g, "n");
                var d = "";
                for (var F = 0; F < k.length; F++) {
                    var x = k.charCodeAt(F);
                    if (x < 128) {
                        d += String.fromCharCode(x)
                    } else {
                        if ((x > 127) && (x < 2048)) {
                            d += String.fromCharCode((x >> 6) | 192);
                            d += String.fromCharCode((x & 63) | 128)
                        } else {
                            d += String.fromCharCode((x >> 12) | 224);
                            d += String.fromCharCode(((x >> 6) & 63) | 128);
                            d += String.fromCharCode((x & 63) | 128)
                        }
                    }
                }
                return d
            }

            var C = Array();
            var P, h, E, v, g, Y, X, W, V;
            var S = 7, Q = 12, N = 17, M = 22;
            var A = 5, z = 9, y = 14, w = 20;
            var o = 4, m = 11, l = 16, j = 23;
            var U = 6, T = 10, R = 15, O = 21;
            s = J(s);
            C = e(s);
            Y = 1732584193;
            X = 4023233417;
            W = 2562383102;
            V = 271733878;
            for (P = 0; P < C.length; P += 16) {
                h = Y;
                E = X;
                v = W;
                g = V;
                Y = u(Y, X, W, V, C[P + 0], S, 3614090360);
                V = u(V, Y, X, W, C[P + 1], Q, 3905402710);
                W = u(W, V, Y, X, C[P + 2], N, 606105819);
                X = u(X, W, V, Y, C[P + 3], M, 3250441966);
                Y = u(Y, X, W, V, C[P + 4], S, 4118548399);
                V = u(V, Y, X, W, C[P + 5], Q, 1200080426);
                W = u(W, V, Y, X, C[P + 6], N, 2821735955);
                X = u(X, W, V, Y, C[P + 7], M, 4249261313);
                Y = u(Y, X, W, V, C[P + 8], S, 1770035416);
                V = u(V, Y, X, W, C[P + 9], Q, 2336552879);
                W = u(W, V, Y, X, C[P + 10], N, 4294925233);
                X = u(X, W, V, Y, C[P + 11], M, 2304563134);
                Y = u(Y, X, W, V, C[P + 12], S, 1804603682);
                V = u(V, Y, X, W, C[P + 13], Q, 4254626195);
                W = u(W, V, Y, X, C[P + 14], N, 2792965006);
                X = u(X, W, V, Y, C[P + 15], M, 1236535329);
                Y = f(Y, X, W, V, C[P + 1], A, 4129170786);
                V = f(V, Y, X, W, C[P + 6], z, 3225465664);
                W = f(W, V, Y, X, C[P + 11], y, 643717713);
                X = f(X, W, V, Y, C[P + 0], w, 3921069994);
                Y = f(Y, X, W, V, C[P + 5], A, 3593408605);
                V = f(V, Y, X, W, C[P + 10], z, 38016083);
                W = f(W, V, Y, X, C[P + 15], y, 3634488961);
                X = f(X, W, V, Y, C[P + 4], w, 3889429448);
                Y = f(Y, X, W, V, C[P + 9], A, 568446438);
                V = f(V, Y, X, W, C[P + 14], z, 3275163606);
                W = f(W, V, Y, X, C[P + 3], y, 4107603335);
                X = f(X, W, V, Y, C[P + 8], w, 1163531501);
                Y = f(Y, X, W, V, C[P + 13], A, 2850285829);
                V = f(V, Y, X, W, C[P + 2], z, 4243563512);
                W = f(W, V, Y, X, C[P + 7], y, 1735328473);
                X = f(X, W, V, Y, C[P + 12], w, 2368359562);
                Y = D(Y, X, W, V, C[P + 5], o, 4294588738);
                V = D(V, Y, X, W, C[P + 8], m, 2272392833);
                W = D(W, V, Y, X, C[P + 11], l, 1839030562);
                X = D(X, W, V, Y, C[P + 14], j, 4259657740);
                Y = D(Y, X, W, V, C[P + 1], o, 2763975236);
                V = D(V, Y, X, W, C[P + 4], m, 1272893353);
                W = D(W, V, Y, X, C[P + 7], l, 4139469664);
                X = D(X, W, V, Y, C[P + 10], j, 3200236656);
                Y = D(Y, X, W, V, C[P + 13], o, 681279174);
                V = D(V, Y, X, W, C[P + 0], m, 3936430074);
                W = D(W, V, Y, X, C[P + 3], l, 3572445317);
                X = D(X, W, V, Y, C[P + 6], j, 76029189);
                Y = D(Y, X, W, V, C[P + 9], o, 3654602809);
                V = D(V, Y, X, W, C[P + 12], m, 3873151461);
                W = D(W, V, Y, X, C[P + 15], l, 530742520);
                X = D(X, W, V, Y, C[P + 2], j, 3299628645);
                Y = t(Y, X, W, V, C[P + 0], U, 4096336452);
                V = t(V, Y, X, W, C[P + 7], T, 1126891415);
                W = t(W, V, Y, X, C[P + 14], R, 2878612391);
                X = t(X, W, V, Y, C[P + 5], O, 4237533241);
                Y = t(Y, X, W, V, C[P + 12], U, 1700485571);
                V = t(V, Y, X, W, C[P + 3], T, 2399980690);
                W = t(W, V, Y, X, C[P + 10], R, 4293915773);
                X = t(X, W, V, Y, C[P + 1], O, 2240044497);
                Y = t(Y, X, W, V, C[P + 8], U, 1873313359);
                V = t(V, Y, X, W, C[P + 15], T, 4264355552);
                W = t(W, V, Y, X, C[P + 6], R, 2734768916);
                X = t(X, W, V, Y, C[P + 13], O, 1309151649);
                Y = t(Y, X, W, V, C[P + 4], U, 4149444226);
                V = t(V, Y, X, W, C[P + 11], T, 3174756917);
                W = t(W, V, Y, X, C[P + 2], R, 718787259);
                X = t(X, W, V, Y, C[P + 9], O, 3951481745);
                Y = K(Y, h);
                X = K(X, E);
                W = K(W, v);
                V = K(V, g)
            }
            var i = B(Y) + B(X) + B(W) + B(V);
            return i.toLowerCase()
        };

        var size = size || 80;

        return 'http://www.gravatar.com/avatar/' + MD5(email) + '.jpg?s=' + size;
    },
    parseHTML: function (str) {
        let div = document.createElement('div');
        div.innerHTML = str;
        return div.children[0];
    },
    getFromURL: function (name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    closeModal: function (id) {
        let modal = document.getElementById(id);
        modal.classList.remove('is-active');
    }
}

function usePlaceholder (e, url) {
    e.src = url || '/images/placeholder.png';
    e.onerror = ""
}