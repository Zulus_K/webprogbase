let router = require('express').Router();
const storage_users = require("@DB/controller.users");
const auth = require('@auth');
const utils = require('@utils');

router.get('/registration', async function (req, res, next) {
    try {
        res.render('registration', {});
    } catch (e) {
        utils.throwError(next, 500)
    }
});

router.get('/login', async (req, res, next) => {
    res.render('login', {});
})

module.exports = router;




