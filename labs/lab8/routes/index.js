"use strict";
const express=require('express');
let router = require('express').Router();
const DBpublications = require("@DB/controller.publications");
const utils = require("@utils");
const path=require('path');
const config=require('@config')
/* GET home page. */
router.get('/', async function (req, res, next) {
    try {
        res.render('index');
    } catch (e) {
        console.log(e);
        throw e;
    }
});
module.exports = router;
