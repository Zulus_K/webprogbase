"use strict";
let router = require('express').Router();
const DBpublication = require('@DB/controller.publications');
const DBimage = require('@DB/controller.image');
const DBuser = require('@DB/controller.users');
const utils = require('@utils');
const auth = require('@auth');


router.get('/new', function (req, res, next) {
    try {
        res.render("publication-create",{});
    } catch (err) {
        console.log(err);
        return utils.throwError(next, 404);
    }
});

router.get('/:id', async function (req, res, next) {
    res.render('publication', {});
});

router.get('/', async function (req, res, next) {
    try {
        res.render('publications');
    } catch (e) {
        console.log(e);
        next();
    }
});


module.exports = router;
