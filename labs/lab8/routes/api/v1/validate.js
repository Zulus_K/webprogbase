// get res by id
// remove res
// create res
"use strict";
const express = require('express');
let router = express.Router();
const Utils = require('@utils');
const DBusers = require('@DB/controller.users')
const config = require('@config');

router.post('/registration/email', async (req, res, next) => {
    console.log(req.body);
    try {
        if (req.body.value) {
            //check is email valid
            if (!config.validate('user', 'email', req.body.value.trim())) {
                return res.json({success: true, result: false, message: "Invalid email, email should be you@host"})
            }
            if (await DBusers.findByEmail(req.body.value.trim())) {
                return res.json({success: true, result: false, message: "Email is already taken"})
            }
            else return res.json({success: true, result: true})
        } else {
            return Utils.errorAPI(res, 400, "Bad arguments");
        }
    } catch (e) {
        return Utils.errorAPI(res, 500, "Server error");
    }
})
router.post('/registration/password', async (req, res, next) => {
    console.log(req.body);
    try {
        if (req.body.value) {
            //check is email valid
            if (!config.validate('user', 'password', req.body.value.trim())) {
                return res.json({
                    success: true,
                    result: false,
                    message: "Please, use only characters A-z, 0-9 in your password, it should contain more than 8 symbols"
                })
            }
            else return res.json({success: true, result: true})
        } else {
            return Utils.errorAPI(res, 400, "Bad arguments");
        }
    } catch (e) {
        return Utils.errorAPI(res, 500, "Server error");
    }
})
router.post('/registration/fullname', async (req, res, next) => {
    console.log(req.body);
    try {
        if (req.body.value) {
            //check is email valid
            if (!config.validate('user', 'fullname', req.body.value.trim())) {
                return res.json({
                    success: true,
                    result: false,
                    message: "Please, use only letters in your name, it should starts from caps letter'"
                })
            }
            else return res.json({success: true, result: true})
        } else {
            return Utils.errorAPI(res, 400, "Bad arguments");
        }
    } catch (e) {
        return Utils.errorAPI(res, 500, "Server error");
    }
})
router.post('/login/email', async (req, res, next) => {
    console.log(req.body);
    try {
        if (req.body.value) {
            //check is email valid
            if (!config.validate('user', 'email', req.body.value.trim())) {
                return res.json({success: true, result: false, message: "Invalid email, email should be you@host"})
            }
            else return res.json({success: true, result: true})
        } else {
            return Utils.errorAPI(res, 400, "Bad arguments");
        }
    } catch (e) {
        return Utils.errorAPI(res, 500, "Server error");
    }
})
router.post('/login/password', async (req, res, next) => {
    try {
        if (req.body.value) {
            //check is email valid
            if (!config.validate('user', 'password', req.body.value.trim())) {
                return res.json({
                    success: true,
                    result: false,
                    message: "Please, use only characters A-z, 0-9 in your password, it should contain more than 8 symbols"
                })
            }
            else return res.json({success: true, result: true})
        } else {
            return Utils.errorAPI(res, 400, "Bad arguments");
        }
    } catch (e) {
        return Utils.errorAPI(res, 500, "Server error");
    }
})
router.post('/publication/:name', async (req, res, next) => {
    try {
        if (req.body.value) {
            if (!config.validate('publication', req.params.name, req.body.value.trim())) {
                return res.json({success: true, result: false, message: "Invalid, fix it"})
            }
            else return res.json({success: true, result: true})
        } else {
            return Utils.errorAPI(res, 400, "Bad arguments");
        }
    } catch (e) {
        return Utils.errorAPI(res, 500, "Server error");
    }
})

router.use((req, res, next) => {
    return res.json({success: true, result: true});
})
module.exports = router;