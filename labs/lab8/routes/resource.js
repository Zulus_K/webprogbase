"use strict";
let router = require('express').Router();
const DBpublication = require("@DB/controller.publications");
const DBimage = require("@DB/controller.image");


router.get('/images/:id', async function (req, res, next) {
    try {
        let image = await DBimage.getById(req.params.id);
        if(image) {
            res.type(image.filename.split('.').pop());
            console.log(`+:api get image ${req.params.id}`);
            return (await image.stream).pipe(res);
        }
    } catch (e) {
        console.log(e);
    }
    next();
});
module.exports = router;

