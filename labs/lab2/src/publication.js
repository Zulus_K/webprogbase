"use strict";
let fs = require('fs');

let STORAGE_NAME="publications.json";

function saveStorage(path, storage) {
    return new Promise((resolve, reject) => {
        storage.lastUpdate = new Date().toISOString();
        fs.writeFile(path, JSON.stringify(storage), (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve("Saved");
                }
            }
        );
    });
}

function Storage() {
    this.currentID = 0;
    this.lastUpdate = new Date().toISOString();
    this.items = [];
}

function getStorage(path) {
    return new Promise((resolve) => {
        fs.readFile(path, (err, data) => {
            if (err) {
                resolve(new Storage());
            } else {
                try {
                    let storage = JSON.parse(data.toString());
                    resolve(storage);
                } catch (e) {
                    resolve(new Storage());
                }
            }
        });
    });
}

function Publication(name, author, date, tags, difficult, text) {
    this.name = name;
    this.author = author;
    this.date = new Date(date);
    this.tags = tags.slice();
    this.difficult = difficult;
    this.text = text;
}

function create(name, author, date, tags, difficult, text) {
    return new Promise((resolve, reject) => {
        let x = new Publication(name, author, date, tags, difficult, text);
        getStorage(STORAGE_NAME).then((storage) => {
            x.id = storage.currentID++;
            storage.items.push(x);
            saveStorage(STORAGE_NAME, storage)
                .then(() => resolve(JSON.stringify(x)))
                .catch((err) => reject("CANNOT SAVE UPDATED STORAGE " + err));
        });
    });
}

function getAll() {
    return new Promise((resolve, reject) => {
        getStorage(STORAGE_NAME)
            .then((storage) => {
                    resolve(storage.items);
                }
            )
            .catch(() => reject("CANNOT LOAD STORAGE"));
    });
}

function findIndex(id, storage) {
    return storage.items.findIndex((e) => {
        if (Number(e.id) === id) {
            return true;
        }
    });
}

function getById(id) {
    return getStorage(STORAGE_NAME)
        .then((storage) => {
                let index = findIndex(id, storage);
                if (index === -1) {
                    return Promise.reject("NO SUCH ID " + id);
                } else {
                    return storage.items[index];
                }
            }
        )
        .catch(() => Promise.reject("CANNOT LOAD STORAGE"));
}

function update(id, property, value) {
    return new Promise((resolve, reject) => {
        //check is valid property name
        if (["", "id", "date"].indexOf(property.toLowerCase()) >= 0) {
            reject("INVALID PROPERTY NAME '" + property + "'");
        }
        if (value === undefined) {
            reject("EMPTY VALUE");
        }
        getStorage(STORAGE_NAME)
            .then((storage) => {
                    let index = findIndex(id, storage);
                    if (index === -1) {
                        reject("NO SUCH ID " + id);
                    }else if(storage.items[index][property.toLowerCase()]===undefined){
                    	reject("INVALID PROPERTY NAME");
                    } else {
                        if (property.toLowerCase() === "tags") {
                            storage.items[index]["tags"] = value.split(" ");
                        } else {
                            storage.items[index][property.toLowerCase()] = value;
                        }
                        saveStorage(STORAGE_NAME, storage).then(() => {
                            resolve("UPDATED");
                        }).catch(() => {
                            reject("CANNOT SAVE STORAGE");
                        });
                    }
                }
            )
            .catch(() => reject("CANNOT LOAD STORAGE"));
    });
}

function remove(id) {
    return new Promise((resolve, reject) => {
        getStorage(STORAGE_NAME).then((storage) => {
                let index = findIndex(id, storage);
                if (index === -1) {
                    reject("NO SUCH ID");
                } else {
                    storage.items[index] = storage.items[storage.items.length - 1];
                    storage.items.pop();
                    saveStorage(STORAGE_NAME, storage).then(() => {
                        resolve("REMOVED");
                    }).catch(() => {
                        reject("CANNOT SAVE STORAGE");
                    });
                }
            }
        ).catch(() => reject("CANNOT LOAD STORAGE"));
    });
}

module.exports = {
    create: create,
    getById: getById,
    update: update,
    remove: remove,
    getAll: getAll
};