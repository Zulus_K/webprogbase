"use strict";
let express = require('express');
let router = express.Router();
let storage = require("../storage/storage");
let fs = require('fs-promise');
const itemsPerPage = 7;
const rangeOfpage = 4;

router.get('/:id', function (req, res, next) {
    storage.getById(Number(req.params.id)).then(item => {
        console.log(item.title);
        res.render('publication', item);
    }).catch(() => {
        next();
    });
});
router.get('/new', function (req, res, next) {
    try {
        res.render("newPublication")
    } catch (err) {
        Console.log("Error in ", new Error().stack);
    }
});

router.get('/', async function (req, res, next) {
    try {
        if (!req.query.page) {
            req.query.page = 1;
        }
        console.log(`page=${req.query.page} name=${req.query.title}`);

        let args = paginate(await getDataByQuery(req.query), req.query.page);
        args.startURL = "/publications?" + ((req.query.title) ? `title=${req.query.title}&` : "");
        args.searchName = (req.query.title) ? req.query.title : "";
        res.render('publications_list', args);
    } catch (e) {
        console.log(e);
        next();
    }
});

async function getDataByQuery(query) {
    let items = await storage.getAll();
    if (query.title && query.title !== '') {
        return items.filter((e) => {
            return e.title.toLowerCase().startsWith(query.title.toLowerCase());
        });
    } else {
        return items;
    }
}

function paginate(items, page) {
    let args = {};
    let start = (page - 1) * itemsPerPage;
    if (start > items.length || start < 0) {
        throw "Page out of bounds";
    }
    args.pageCount = Math.trunc(items.length / itemsPerPage) + ((items.length % itemsPerPage > 0) ? 1 : 0);
    args.items = items.slice(start, start + itemsPerPage);
    args.currentPage = page;
    args.range = rangeOfpage;
    return args;
}

router.post("/add", async (req, res, next) => {
    // console.log("POST METHOD");
    console.log(JSON.stringify(req.body));


    //create new publication
    try {
        let result = await storage.create(
            req.body.title,
            req.body.author,
            req.body.link,
            Date.now(),
            await storage.uploadImage(req.files.logo),
            await parseTags(req.body),
            req.body.difficult,
            req.body.description,
            req.body.text);
    } catch (e) {
        console.log("Error at create" + e);
    }
    res.redirect("/publications");
});

router.post("/remove/:id", (req, res, next) => {
    console.log("\n\n\n" + req.params.id + "\n\n");
    storage.remove(Number(req.params.id)).then(() => {
        console.log(`REMOVED ${req.params.id}`)
    }).catch(e => {
        console.log(`ERROR AT REMOVE${e}`)
    });
    res.redirect("/publications");
});
module.exports = router;

//parse tags from request
async function parseTags(body) {
    let tags = [];
    for (let i = 0; ("tag" + i) in body; i++) {
        let val = body["tag" + i];
        if (val.length > 0) {
            tags.push(val);
        }
    }
    return tags;
}