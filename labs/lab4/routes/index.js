"use strict";
let express = require('express');
let router = express.Router();
let storage = require("../storage/storage");
let commonmark = require('commonmark');
/* GET home page. */
router.get('/', function (req, res) {
    storage.getAll().then(items => {
        let data = {number_of_publications: items.length};
        res.render('index', data);
    })
});
router.get('/test', function (req, res) {
    res.render('test', {});
});
module.exports = router;
