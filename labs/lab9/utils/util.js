const DBpublication = require('@DB/controller.publications');
const DBimage = require('@DB/controller.image');
const DBuser = require('@DB/controller.users');
const auth = require('@auth');
const crypto = require('crypto');
const ObjectID = require('mongoose').Types.ObjectId;
const fs = require('fs-promise');
const commonmark = require('commonmark');


exports.crypto = {
    hash: (data) => {
        const hash = crypto.createHmac('sha512', process.env.SALT);
        hash.update(data);
        const value = hash.digest('hex');
        return value;
    },
    compare: (plainData, hash) => {
        return this.crypto.hash(plainData) === hash;
    }
}
exports.emptyId = () => {
    return new ObjectID();
}
exports.paginateOptions = function (page, limit, sort) {
    return {
        page: (Number(page) && Number(page) >= 0) ? Number(page) : 1,
        limit: (Number(limit) && Number(limit) >= 0) ? Number(limit) : 20,
        sort: sort || {}
    };
}

exports.isValidID = function (id) {
    return ObjectID.isValid(id);
}
exports.str2id = function (str) {
    if (typeof str != 'string') str;
    try {
        return new ObjectID.createFromHexString(str);
    } catch (e) {
        return new ObjectID();
    }
}

exports.parseJSON = function (str) {
    try {
        return JSON.parse(str);
    } catch (e) {
    }
}

exports.convertPagination = function (result, args) {
    args.currentPage = result.page;
    args.range = Number(process.env.RANGE);
    args.items = result.docs;
    console.log(result)
    args.pageCount = result.pages;
    return args;
}

exports.errorAPI = (res, code, message) => {
    return res
        .status(code)
        .json({success: false, message: message})
        .end();
}

exports.md = function (text) {
    var reader = new commonmark.Parser();
    var writer = new commonmark.HtmlRenderer();
    var parsed = reader.parse(text); // parsed is a 'Node' tree
// transform parsed if you like...
    return writer.render(parsed); // result is a String
}

exports.readFile = function (path) {
    return fs.readFile(path, 'utf8');
}