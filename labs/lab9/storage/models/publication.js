const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const date = require('dateformat')
const DBimage = require('@DB/controller.image');

const PublicationSchema = new Schema({
    title: {type: String, default: "New publication", required: true},
    author_id: Schema.Types.ObjectId,
    difficult: {type: Number, default: 1},
    image_id: {type: Schema.Types.ObjectId},
    tags: {type: [String], default: []},
    text: {type: String, required: true},
    description: {type: String, default: ""},
    createdAt: {type: Date, default: Date.now},
});

PublicationSchema.plugin(require('mongoose-paginate'));

PublicationSchema.pre('save', function (next) {
    if (this.isNew) {
        this.createdAt = date(new Date(), "ddd mmm dd yyyy HH:MM")
    }
    next();
})
PublicationSchema.pre('remove', async function (next) {
    try {
        if (this.image_id) {
            console.log(`?:try to remove file ${this.image_id}`);
            await DBimage.remove(this.image_id);
            console.log('+:file removed');
        }
    } catch (e) {
        console.log(e);
    }
    next();
})

PublicationSchema.methods.update = async function (values) {
    if (values.title) {
        this.title = values.title;
    }
    if (values.text) {
        this.text = values.text;
    }
    if (values.description) {
        this.description = values.description;
    }
    if (values.difficult) {
        this.difficult = values.difficult;
    }
    if (values.add_tags) {
        values.add_tags.forEach((value) => {
            if (this.tags.indexOf(value) < 0) {
                this.tags.push(value);
            }
        })
    }
    if (values.remove_tags) {
        values.remove_tags.forEach((value) => {
            const index = this.tags.indexOf(value);
            if (index >= 0) {
                this.tags.splice(index, 1);
            }
        })
    }
    if (values.image_id) {
        if (this.image_id) {
            console.log(`?:try to remove file ${this.image_id}`);
            await DBimage.remove(this.image_id);
            console.log('+:file removed');
            this.image_id = values.image_id;
        }
    }
    return this.save();
}
module.exports = mongoose.model('Publication', PublicationSchema);

