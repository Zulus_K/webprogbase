"use strict";
const mongoose = require('mongoose');
const config = require('@config');
const Utils = require('@utils');

//connect to db
exports.connect = async function () {
    const database = mongoose.connection;
    mongoose.Promise = Promise;
    const result = mongoose.connect(config.db, {
        useMongoClient: true,
        promiseLibrary: global.Promise
    });
    database.on('error', error => console.log(`- DB: connection failed: ${error}`));
    database.on('connected', () => console.log(`+ DB:connected`));
    database.on('disconnected', () => console.log('- DB:disconnected'));
    process.on('SIGINT', () => {
        database.close(() => {
            console.log('+DB:connection closed');
            process.exit(0);
        })
    });
    return result;
}
//create new entity
exports.create = function (model, params) {
    let entity = new model(params);
    return new Promise((resolve, reject) => {
        entity.save((err, grid) => {
            if (err) {
                console.log(err);
                reject(err);
            }
            else resolve(grid);
        });
    });
}

exports.getAll = function (model, page, limit, sort) {
    return model.find({}).exec();
}

exports.getById = function ( model,id) {
    if(Utils.isValidID(id)) {
        return model.findById(id);
    }else{
        throw "Invalid id"
    }
}

exports.size = function (model) {
    return model.count();
}

exports.remove = function (model,id) {
    return model.findByIdAndRemove(id).exec();
}

exports.find = function (model, query, page, limit, sort) {
    return model.paginate(query, Utils.paginateOptions(page, limit, sort));
}

exports.findOne = function (model,query) {
    return model.findOne(query)
}