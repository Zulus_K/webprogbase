require('dotenv').config();
require('module-alias/register');
// drivers of DB
const DBimage = require('./storage/controller.image');
const DBdriver = require('./storage/controller');
const DBuser = require('./storage/controller.users');
const DBpublications = require('./storage/controller.users');
// tools
const express = require('express');
const path = require('path');
const logger = require('morgan');
const favicon = require('serve-favicon');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
const cors = require('cors');
// config
const config = require('./utils/config')

// app
app = express();

async function createConnections () {
    try {
        let result = await DBdriver.connect(config.db);
        console.log('+:Connected to DB');
        await  DBimage.connect();
        console.log('+:Connected to image storage ');
    } catch (e) {
        console.log('-:Cannot connected to mongodb ' + (e ? e : ""));
        process.exit(0);
    }
}

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(busboyBodyParser());
app.use(cookieParser());
app.use(cors());
// init passport
require('./utils/auth').init(app);
console.log('+:Passport configured');


app.use('/api/', require('@API'))
app.use('*', (req, res, next) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'))
});

console.log('+:Routes added');

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});
// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    console.log(res.locals.error);
    // render the error page
    res.status(err.status);
    if (err.status == 401 || err.status == 403) {
        res.redirect('/401');
    } else if (err.status == 500) {
        res.render('/500');
    } else {
        res.render('/404');
    }
});

createConnections();

module.exports = app;



