import axios from 'axios';
import store from '@/store/store'

export default () => {
  let args = {};
  args.baseURL = `http://localhost:${process.env.PORT || 2004}`;
  if (store.getters.isLogged()) {
    args.headers = {Authorization: `Bearer ${store.state.tokens.access}`};
  }
  return axios.create(args)
}
