import API from '#/API';
import axios from 'axios';
import store from '@/store/store'
import Utils from '#/Utils';

export default {
  get (args) {
    console.log(args)
    let url = "api/v1/publications?";
    for (let key in args) {
      if (args[key]) {
        url += `${key}=${args[key]}&`;
      }
    }
    return API().get(url);
  },
  uploadFile (file) {
    let form = new FormData();
    form.append('file', file);
    return API().post('api/v1/res', form);
  },
  removeFile (id) {
    return API().delete('api/v1/res', {id: id});
  },
  check (path, key, value) {
    return API().post(`/api/v1/validate/publication/${path}/${key}`, {value: value});
  },
  create (args) {
    let form = new FormData();
    for (var key in args) {
      form.append(key, args[key]);
    }
    return API().post(`/api/v1/publications/`, form);
  },
  removePublication (id) {
    let args = {};
    args.baseURL = `http://localhost:${process.env.PORT || 2004}`;
    if (store.getters.isLogged()) {
      args.headers = {Authorization: `Bearer ${store.state.tokens.access}`};
    }
    args.url = '/api/v1/publications';
    args.method = 'delete';
    args.data = {id: id};
    return axios(args).catch(async (e) => {
      console.log(e)
      if (e.response.status == 401) {
        //try to refresh tokens and make req again
        await Utils.refreshTokens();
        return this.removePublication(id);
      }else{
        throw e;
      }
    })
  }
}
