import API from './API';

export default {
  get (args) {
    let url = "api/v1/users?";
    for (let key in args) {
      url += `${key}=${args[key]}&`;
    }
    return API().get(url);
  }
}
