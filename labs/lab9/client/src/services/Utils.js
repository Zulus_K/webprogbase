import commonmark from 'commonmark';
import API from '#/API';
import APIAuth from '#/Auth';
import store from '@/store/store'

var reader = new commonmark.Parser();
var writer = new commonmark.HtmlRenderer();

export default {
  parseMD (str) {
    var parsed = reader.parse(str);
    return writer.render(parsed);
  },
  getAPI () {
    return API().get('/api/v1');
  },
  refreshTokens: async function () {
    try {
      const response = await APIAuth.refresh();
      console.log(response.data)
      if (response.data.success) {
        store.dispatch('setToken_access', response.data.token);
        return;
      }
    } catch (e) {
      console.log(e)
    }
    store.dispatch('setToken_access', null);
    store.dispatch('setToken_refresh', null);
    store.dispatch('setUser', null);
  }
}
