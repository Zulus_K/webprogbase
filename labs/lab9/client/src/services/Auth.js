import API from '#/API';
import axios from 'axios';
import store from '@/store/store'

export default {
  login (credentials) {
    return API().post('/api/v1/auth/login/', credentials);
  },
  register (credentials) {
    return API().post('/api/v1/auth/register/', credentials);
  },
  check (path, key, value) {
    return API().post(`/api/v1/validate/auth/${path}/${key}`, {value: value});
  },
  refresh () {
    let args = {};
    args.baseURL = `http://localhost:${process.env.PORT || 2004}`;
    args.url = '/api/v1/auth/token';
    args.headers = {Authorization: `Bearer ${store.state.tokens.refresh}`};
    args.method = 'GET';
    return axios(args);
  }
}
