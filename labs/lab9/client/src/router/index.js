import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/index'
import Publications_list from '@/components/Publications/Publications'
import Publications_view from '@/components/Publications/Publications.view'
import Publications_create from '@/components/Publications/Publications.create'
import API from '@/components/Other/API'
import Page404 from '@/components/Other/404'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '*',
      name: '404',
      component: Page404
    },
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/publications',
      name: 'Publications',
      component: Publications_list
    },
    {
      path: '/publications/new',
      name: 'Publications-create',
      component: Publications_create
    },
    {
      path: '/publications/:id',
      name: 'Publications-view',
      component: Publications_view
    },
    {
      path: '/api/v1/docs',
      name: 'API',
      component: API
    }
  ]
})
