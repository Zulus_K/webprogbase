import Vue from 'vue'
import App from './App'
import router from './router'
// stores
import {sync} from 'vuex-router-sync'
import store from '@/store/store'
// config
import config from '@/services/config';
// styles
import 'vue-material-design-icons/styles.css'
import '@/assets/styles/main.scss'
import Vuetify from 'vuetify'
import 'font-awesome/css/font-awesome.css'
import VeeValidate from 'vee-validate';
import Gravatar from 'vue-gravatar';

config()

Vue.component('v-gravatar', Gravatar);
import('vuetify/dist/vuetify.min.css')

Vue.use(VeeValidate);

sync(store, router)

Vue.config.productionTip = false
Vue.use(Vuetify, {
  theme: {
    primary: 'rgb(33,33,33)'
  }
});
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {App}
})
