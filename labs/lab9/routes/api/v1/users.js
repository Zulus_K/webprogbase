// todo
// get users by query
// remove users
// update users

"use strict";
const express = require('express');
let router = express.Router();
const Utils = require('@utils');
const DBusers = require('@DB/controller.users')
const config = require('@config');
const auth = require('@auth')
let tools = {
    collect: {
        find: (req) => {
            return {
                page: Number(req.query.page) || 1,
                limit: Number(req.query.limit) || config.LIMIT,
                query: tools.query.find(req.query)
            }
        },
        delete: (req) => {
            return {
                query: tools.query.delete(req.body)
            }
        },
        put: (req) => {
            return {
                query: tools.query.put(req.body)
            }
        },
        post: (req) => {
            return {
                query: tools.query.post(req.body)
            }
        }
    },
    check: {
        find: (args) => {
            if (!args.page || !Number(args.page) || Number(args.page) <= 0) {
                return false;
            }
            if (!args.limit || !Number(args.limit) || Number(args.limit) <= 0) {
                return false;
            }
            if (!args.query) return false;
            return true;
        },
        delete: (args, user) => {
            return args.query.id && (user.isAdmin || String(user._id) == String(args.query.id));
        },
        put: (args, user) => {
            return tools.check.delete(args, user) && Object.keys(args.query.values).length > 0 && (!('isAdmin' in args.query) || user.isAdmin);
        }
    },
    query: {
        find: (req) => {
            let query = {}
            //title
            if (req.fullname) {
                query.fullname = new RegExp(`^${req.fullname.trim()}`, "i");
            }
            //publications
            if (req.publications) {
                let publications = {$in: []};
                let parsed = Utils.parseJSON(req.publications);
                if (Array.isArray(parsed)) {
                    parsed.forEach((value) => {
                        publications.$in.push(String(value));
                    })
                } else if (parsed) {
                    publications.$in.push(parsed);
                }
                if (parsed) {
                    query.publications = publications;
                }
            }
            //id
            if (req.id && Utils.isValidID(req.id)) {
                query._id = Utils.str2id(req.id);
            }
            //access
            if (req.isAdmin) {
                query.isAdmin = req.isAdmin;
            }
            //email
            if (req.email) {
                query.email = req.email;
            }
            //telegram
            if (req.telegram) {
                query.telegram = req.telegram;
            }
            return query;
        },
        delete: (req) => {
            let query = {};
            if (req.id && Utils.isValidID(req.id)) {
                query.id = Utils.str2id(req.id);
            }
            return query;
        },
        put: (req) => {

            let query = {values: {}};
            // get target
            if (req.target && Utils.isValidID(req.target)) {
                query.id = Utils.str2id(req.target);
            }

            //title
            if (req.fullname) {
                query.values.fullname = req.fullname.trim();
            }
            //access
            if (req.isAdmin) {
                query.values.isAdmin = req.isAdmin;
            }
            //email
            if (req.email) {
                query.values.email = req.email;
            }
            //telegram
            if (req.telegram) {
                query.values.telegram = req.telegram;
            }
            // password
            if (req.password) {
                query.values.password = req.password;
            }
            return query;
        }
    },
    result: {
        find: (args) => {
            return {
                success: true,
                query: args.query,
                page: args.page,
                total: args.total,
                limit: args.limit,
                pages: args.pages,
                items: tools.doc2items(args.docs)
            }
        },
        delete: (args) => {
            return {
                success: true,
                query: args.query,
                user: tools.minimize(args.user)
            }
        },
        update: (args) => {
            return tools.result.delete(args)
        }
    },
    doc2items (docs) {
        if (docs) {
            let items = [];
            docs.forEach((doc) => {
                items.push(tools.minimize(doc));
            });
            return items;
        } else {
            return []
        }
    },
    minimize: (doc) => {
        return {
            fullname: doc.fullname,
            telegram: doc.telegram,
            email: doc.email,
            isAdmin: doc.isAdmin,
            id: doc._id,
            publications: doc.publications,
        }
    }
};

router.route('/')
    .get(async (req, res, next) => {
        const args = tools.collect.find(req);
        if (!tools.check.find(args)) {
            return Utils.errorAPI(res, 400, "Invalid arguments");
        }
        try {
            const result = await DBusers.find(args.query, args.page, args.limit);
            return res.json(tools.result.find(result));
        } catch (e) {
            console.log(e);
            return Utils.errorAPI(res, 500, "Server error");
        }
    })
    .delete(auth.jwt.check.login, async (req, res, next) => {
        const args = tools.collect.delete(req);
        if (!tools.check.delete(args, req.user)) {
            return Utils.errorAPI(res, 400, "Invalid arguments");
        }
        try {
            args.user = await DBusers.remove(args.query.id);
            if (args.user) {
                return res.json(tools.result.delete(args));
            } else {
                return Utils.errorAPI(res, 404, "No such user");
            }
        } catch (e) {
            console.log(e);
            return Utils.errorAPI(res, 500, "Server error");
        }
    })
    .put(auth.jwt.check.login, async (req, res, next) => {
        const args = tools.collect.put(req);
        if (!tools.check.put(args, req.user)) {
            return Utils.errorAPI(res, 400, "Invalid arguments");
        }
        try {
            args.user = await DBusers.getById(args.query.id);
            if (args.user) {
                if (!await args.user.update(args.query.values)) {
                    return Utils.errorAPI(res, 400, "Bad arguments");
                } else {
                    return res.json(tools.result.delete(args));
                }
            } else {
                return Utils.errorAPI(res, 404, "No such user");
            }
        } catch (e) {
            console.log(e);
            if(e.code && e.code==config.errors.INVALID_VALUE){
                return Utils.errorAPI(res, 400, e.message||"Invalid value");
            }else{
                return Utils.errorAPI(res, 500, "Server error");
            }
        }
    })


module.exports = router;