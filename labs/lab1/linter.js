"use strict";
var fs = require('fs'),
    html5Lint = require('html5-lint');


function getErrors(name, err, html) {
    if (err)
        throw err;
    html5Lint(html, function (err, results) {
        let isNamed = false;
        results.messages.forEach(function (msg) {
            if (!isNamed) {
                console.log("---------------------------------------------");
                console.log("ERROR AT " + name);
            }
            isNamed = true;
            let type = msg.type, // error or warning
                message = msg.message;
            console.log("HTML5 Lint [%s]: %s", type, message);
        });
    });
}


let arr = ["index.html", "items/publication1.html", "items/publication2.html", "items/publication3.html", "items/publication4.html", "items/publication5.html", "items/publication5.html"];
for (let name of arr) {
    fs.readFile(name, 'utf8', (err, html) => getErrors(name, err, html));
}
